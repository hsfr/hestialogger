//
//  HestiaLogger.h
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 04/10/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for HestiaLogger.
FOUNDATION_EXPORT double HestiaLoggerVersionNumber;

//! Project version string for HestiaLogger.
FOUNDATION_EXPORT const unsigned char HestiaLoggerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HestiaLogger/PublicHeader.h>


