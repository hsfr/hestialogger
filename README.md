Hestia Logger
=============

The Hestia Logger is a light-weight logging framework for Swift 4 similar to
*log4swift* (it uses a similar XML configuration file). It supports both Cocoa based
and terminal based applications. The latter types do not use Frameworks and so
require a slightly modified way of including the logger. Note that Hestia is
only designed to be used in a Mac OS-X environment not iOS.

Author: Hugh Field-Richards (hsfr@hsfr.org.uk)
 
Date: December 2021 (Version 3.0b1168)

Hestia has a reasonable set of features which make logging simple to use over a range of Cocoa and
terminal based applications:

* Easy to use for the simplest cases.
* Works in a terminal only environment.
* Logging to Xcode console.
* Logging to window within application (only non-terminal apps)
* Output to XML format file.
* Rolling file output.
* Setting and testing log levels in code.
* Limited in-code reconfiguration.
* Uses same XML format configuration as *log4j*.
* Schema support for configuration file using RELAX-NG and Schematron.

It does not support the following:

* Network Logging (using `NSLogger/Users/hsfr/Library/Logs/HestiaTest/` ).
* Asynchronous logging.
* Auto-reload of configuration.
* Logging parameters using closures.

It is intended that some of the above may be added in future versions, if there is a need.

`LoggerFactory` is the primary class for the logging system.
Always make sure that you instantiate this class
to use it. 

## Logging Concepts

Levels determine whether a particular logging statement is successful.
There are 7 levels (identical to *log4j*):

*  trace = 0
*  debug = 1
*  info = 2
*  warn = 3
*  error = 4
*  fatal = 5
*  off = 6

The logger level determines that all logging statements which have an
associated value less than the level will not be output. For example
if a logger's level is defined as `warn` then a `logger.info()`
statement will be ignored. The `off` level is a convenience to
inhibit a logger either from the configuration
file or set in the code.

### Loggers

Application code writes the log using a logger which are the 
primary means to generate logs. Each logger has an associated 
level which controls whether that log succeeds. Loggers are 
generally identified by a simple name, but it is suggested 
that the name of the class in which they are declared is used. 
If the named logger cannot be found a generic “root” logger 
is used (and therefore must be defined in the configuration file).

### Appenders

Each logger has one or more attached appender that are used to write the log message to the appropriate destination, file or console, in a variety of formats. They are identified by a unique string in the configuration file.

### Formatters

It is important that messages output can be formatted in a user defined manner. Each appender has an associated formatter that controls this. A single formatter may be used by several appenders and are identified by a unique string. Formatters are predefined and, other than parameters passed to them when they are used, are not user changeable.

## Configuration

The heart of describing the logging process is the Configuration File. 
This is an XML formatted file that must be loaded before 
the logging system is used. The overall structure of the config file is:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <!-- List of appenders -->
   </appenders>
   
   <loggers>
      <!-- root logger definition -->
      <!-- List of loggers -->
   </loggers>
   
</configuration>
```

This is virtually identical to that used by *log4java*. There are two schema files 
supplied with the framework and source code, `hestia.rng` and `hestia.sch` 
which can be used to check the configuration files. Note that the Schematron 
file is extensible and currently only represents a bare minimum.

If the schemas are used within a suitable editor, such as oXygen ([https://www.oxygenxml.com]()), 
then the configuration file should be changed to the following or
equivalent to support your editor of choice:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<?oxygen RNGSchema="hestia.rng" type="xml"?>
<?oxygen SCHSchema="hestia.sch"?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
```

## Defining Formatters

Currently there are 3 formatters:

* SimpleLayout
* PatternLayout
* XmlLayout

### SimpleLayout

The simplest output layout.

```xml
<layout class="SimpleLayout"/>
```

The error messages are formatted in the form `level - message`, for example:

```
Info - Logger initialised
```


### PatternLayout

The most common layout allowing the user to control what is output, for example

```xml
<layout class="PatternLayout">
    <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: [%p] %F:%M:%L: %m"/>
</layout>
```

where

```
%d{} - formatted date
%F   - file name containing the log statement
%M   - function name where log statement occurs
%p   - log level
%L   - line number within file where log statement occurs
%m   - log message
```

would give (line break inserted for clarity)


     06/01/2020 07:19: [Debug] AppDelegate.swift:applicationDidFinishLaunching(_:):42: \
         Initialised logger V2.0b310



###XmlLayout

Output the log message formatted as a XML scrap.

```xml
<layout class="XmlLayout"/>
```

The formatting is:

```xml
<log-event date="dd/mm/yyyy hh:mm">
   <data name="file name" line="line number of log"
         function="function where log is used" level="log level"/>
   <message>Logging message</message>
</log-event>
```

For example, two consecutive log messages:


```xml
<log-event date="01/13/2018 05:31">
   <data name="main.swift" line="24" function="setLoggingSystem()" level="Debug"/>
   <message>Logger initialised</message>
</log-event>
<log-event date="01/13/2018 05:31">
   <data name="main.swift" line="26" function="setLoggingSystem()" level="Debug"/>
   <message>Version: Unknown version</message>
</log-event>
```

There are important caveats here: the output is in the form of a single XML tagged statement. It is not
a well-formed XML document. In order to construct a well-formed document
an opening a closing tag must surround the output. When an appender is started,
either to a file or the console, the logger checks to see whether this is the first time it has been written to (when not
appending to the logger). If it is the first write then an opening tag is writing to the logger:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="hestiaXmlOutput.rng" type="xml"?>
<logOutput xmlns="http://www.hsfr.org.uk/Schema/Hestia">
```

The RNG schema reference is merely a convenience and is not really necessary. When any of the files or console
output are closed (implicitly through application exit) the following closing tag is output:

```xml
</logOutput>
```

## Defining Appenders

Currently there are 5 appenders:


* NullAppender
* StdOutAppender
* FileAppender
* RollingFileAppender
* WindowAppender

### NullAppender

Does nothing and "eats" the error message.

```xml
<appender name="null" class="NullAppender"/>
```

### StdOutAppender

Outputs the log message to the console (only suitable for debugging in 
Xcode or from within a terminal app). Foe example:

```xml
<appender name="console" class="StdOutAppender">
    <layout class="PatternLayout">
        <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m"/>
    </layout>
</appender>
```

###FileAppender

 Writes output to a single named file. Each session overwrites the file (no session append)
 and the file can grow to any size.
 
 Note that if the *XmlLayout* format is used an XML header is written out
 first to satisfy the requirements for well-formedness. The also
 requires that the logger is closed to ensure an XML footer
 (closing tag) is output; this is the responsibility of the
 application designer.
 
```xml
<appender name="appender-name" class="FileAppender">
    <param name="append" value="true if appending, otherwise false for new file"/>
    <param name="file" value="name of log file"/>
    <!-- layout definition -->
</appender>
```

For example

```xml
<appender name="xml" class="FileAppender">
    <param name="append" value="true"/>
    <param name="file" value="hestia-log.xml"/>
    <layout class="XmlLayout"/>
</appender>
```

###RollingFileAppender

Writes output to a file giving a choice of appending (or not).
When file reaches a fixed maximum size it is pushed onto a
rolling stack of files `*.1`, `*.2` etc. The number of
overspill files is set in the configuration file.

```xml
<appender name="appender-name" class="RollingFileAppender">
    <param name="append" value="true if appending, otherwise false for new file"/>
    <param name="maxFileSize" value="max size of file in bytes"/>
    <param name="maxBackupIndex" value="number of overspill files"/>
    <param name="file" value="name of log file"/>
    <!-- layout definition -->
</appender>
```

For example

```xml
<appender name="file" class="RollingFileAppender">
    <param name="append" value="false"/>
    <param name="maxFileSize" value="10000"/>
    <param name="maxBackupIndex" value="10"/>
    <param name="file" value="hestia.log"/>
    <layout class="PatternLayout">
        <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m"/>
    </layout>
</appender>
```

Note that if the *append* flag is set to false then the log file defined by *file}
is deleted together with all the related overspill files. In the above
example all files matching the pattern `hestia.log` and `hestia.log.*` will be deleted.

If appending then the logging will resume to the file `hestia.log`.

### WindowAppender

(introduced with Version 2.0)

The `WindowAppender` is designed to support an in-application logging window. It was
introduced for those occasions when released applications do not have the benefit of 
running under Xcode. Note that there are some restrictions, the chief of which is it 
cannot be accessed from background tasks. This will be sorted out in future versions.

![Window Appender](WindowAppender-1.png "Window Appender")

```xml
<appender name="logWindow" class="WindowAppender">
   <layout class="PatternLayout">
      <param name="ConversionPattern"
             value="%d{MM/dd/yyyy hh:mm}: [%p] %F:%M:%L: %m"/>
   </layout>
</appender>
```
The options in the window are choice of logger and their associated level. 

![Window Appender](WindowAppender-2.png "Window Appender")

The example above shows the relationship between the logger definition and its value 
by selecting the appropriate class from the pull-down menu. The initial level in the
configuration file is preset into the associated selector.

## Defining Loggers

Loggers are the primary way the code accesses the logging system.
There are two classes of logger definition: a *root* logger
and a list of user defined loggers.

```xml
<loggers>
    <!-- root logger definition -->    
    <!-- list of use defined loggers -->
</loggers>
```

The root logger is required and provides a logger if the user defined logger is not found.
If the root logger is not found
an exception will be raised. For example

```
   func setLoggingSystem() {
      do {
         loggerFactory = LoggerFactory.sharedInstance
         let filePath = "[path to configuration file]"
         let fileName = "hestia.xml"
         try loggerFactory.configure( from: fileName, inFolder: filePath )
         logger = try loggerFactory.getLogger( name: "[logger name]" )
      } catch let e as LoggerError {
         print( e.description )
      } catch {
         _ = displayFatalError( question: "Unknown error when setting logging system", text: "\(error)" )
      }
   }
```

### Root Logger

The root logger defines a “fallback” logger when the requested logger is not found.

```xml
<root>
    <level value="level_name"/>
    <appender-ref ref="named_appender"/>
</root>
```

### User Defined Loggers

These loggers are the means by which the code accesses the logging system. The definitions
are similar to the root logger except that each logger has an associated name.

```xml
<logger name="logger_name">
    <level value="level_name"/>
    <appender-ref ref="named_appender"/>
</logger>
```

For example, assuming there is an appender name *console*, we can say:

```xml
<logger name="Test01.console">
    <level value="warn"/>
    <appender-ref ref="console"/>
</logger>
```

## Using Hestia Within Applications 

### Logger Levels

#### Setting and Inspecting Logger Levels

It is possible to set and get the logging level from within the application.

###### Set Level

```
logger.set( level: "debug" )
```

###### Get Level

```
let currentLevel = logger.getLevel() as LoggerLevelEnum
```

where `LoggerLevelEnum` is the enumeration:

```
case trace = 0, debug, info, warn, error, fatal, off
```

##### Check Level

```
if logger.check( level: LoggerLevelEnum.warn ) {
}    
```

The check returns `true` if the current logging level is less than or equal to the input level.

#### Build Version

There is a static variable associated which reports the version and build number:

```
logger.info( "Version: \(LoggerFactory.version)" )
```
    
## Errors

When the logger reports errors it throws a `LoggerError`. The error can be interrogated by using
the error *description* variable in the following structure:
    
```
do {
    // Code that throws LoggerError
} catch let e as LoggerError {
    print( e.description )
} catch {
    print( "Unknown error" )
}
``` 

## Simple example to console

Using the logger is relatively simple. First a configuration file needs to be defined, the simplest could be

```xml
<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
  
  <appenders>
  
      <appender name="console" class="StdOutAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m"/>
         </layout>
      </appender>
  
  </appenders>
  
  <loggers>
  
     <root>
          <level value="fatal"/>
          <appender-ref ref="console"/>
     </root>
  
     <logger name="Test01.console">
          <level value="info"/>
          <appender-ref ref="console"/>
      </logger>
  
  </loggers>
  
</configuration>

```

The application must set up a logger using the `LoggerFactory`. First define
two variables. *loggerFactory* will become an instance of the *LoggerFactory*, and 
the logger that is used follows the *LoggerProtocol*:

```
var loggerFactory: LoggerFactory!
var logger: LoggerProtocol!
```

It is convenient to enclose the setup in a function:

```
func setLoggingSystem() {
     do {
        loggerFactory = LoggerFactory.sharedInstance
        try loggerFactory.configure()
        logger = try loggerFactory.logger( name: "Test01.console" )
   } catch let e as LoggerError {
        print( e.description() )
    } catch {
        print( "Unknown error when setting logging system"  )
    }
}
```
Using the above with a simple output message is trivial:

```
setLoggingSystem()
logger.info( "Logger initialised" )

```

Typical output would be (the line number and file are only typical)

```
01/06/2018 05:29:   Info main.swift [54] infoTest(): Logger initialised
```

# Building Applications using HestiaLogger

The logger can be used in both Cocoa and terminal based programs. However a slightly
different approach must be taken for each.

## Cocoa based applications

Create a normal Cocoa based application in Xcode. Then add the configuration file,
`hestia.xml`, which is the file that describes how the logger should operate.
If you wish to apply a schema to check the contents of this file then include the
`hestia.rng` and `hestia.sch` files. They are not essential but a wise
precaution if you are able to check XML files against a schema.

### Editing the `AppDelegate.swift` File

Import the Hestia library (from the Framework).

```
import Cocoa
import HestiaLogger
```

Declare an instance of the *LoggerFactory*.

```
var loggerFactory: LoggerFactory!
```

The *setLoggingSystem* function sets up the logger:}

```
func setLoggingSystem( for className: String ) -> Logger? {
   do {
        if loggerFactory == nil {
            let embeddedConfigPath = Bundle.main.resourcePath!
            loggerFactory = LoggerFactory.sharedInstance
            try loggerFactory.configure( from: "hestia.xml", inFolder: embeddedConfigPath )
        }
        let logger = try loggerFactory.getLogger( name: className )
        return logger
    } catch let error as LoggerError {
        print( error.description )
        return nil
    } catch {
        print( "Unknown error when setting logging system"  )
        return nil
    }
}
```

Declare a *logger* instance variable in the main class.
It is declared *private* because it is only used within the *AppDelegate*
class.

```
@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    private var logger: Logger!

```

Add the following code into *applicationDidFinishLaunching* function to set
up the logger.

```
    func applicationDidFinishLaunching( _ aNotification: Notification ) {
        if self.logger == nil {
            self.logger = setLoggingSystem( for: self.className )
            guard self.logger != nil else {
                NSApp.terminate(nil)
                return
            }
        }
        let v = LoggerFactory.version
        logger.debug( "Version: \(v)" )
        let test = LoggerOutput()
        test.dump()
    }
```

The *self.className* is used to provide a link to the configuration file. In this case
the value is *LoggerExample.AppDelegate* and a logger can be declared as, for example

```xml
<logger name="LoggerExample.AppDelegate">
    <level value="debug"/>
    <appender-ref ref="console"/>
</logger>
```

Finally close the logger when the application closes.

```
    func applicationWillTerminate(_ aNotification: Notification) {
        try? self.logger.close()
    }
}
```

The following is an example class which is called by the above. It shows a
private logger declared available only in this class.}

```
class LoggerOutput: NSObject {

    fileprivate var logger: Logger!

    override init() {
        super.init()
        if self.logger == nil {
            self.logger = setLoggingSystem( for: self.className )
        }
    }

    func dump() {
        print( "\(className) current level = \(logger.getLevel() as LoggerLevelEnum)" )
        logger.trace( "Trace message" )
        logger.debug( "Debug message" )
        logger.info( "Info message" )
        logger.warn( "Warn message" )
        logger.error( "Error message" )
        logger.fatal( "Fatal message" )

        logger.set(level: "warn" )
        print( "\(className) current level = \(logger.getLevel() as LoggerLevelEnum)" )

        logger.trace( "Trace message" )
        logger.debug( "Debug message" )
        logger.info( "Info message" )
        logger.warn( "Warn message" )
        logger.error( "Error message" )
        logger.fatal( "Fatal message" )
   }

}
```

This will return the following console log messages:

```
05/13/2019 07:05: Debug AppDelegate.swift [59] setLoggingSystem(): Logger initialised
05/13/2019 07:05: Debug AppDelegate.swift [61] setLoggingSystem(): Version: 1.2b168
HestiaTest.AppDelegate current level = debug
05/13/2019 07:05: Debug AppDelegate.swift [32] dump(): Debug message
05/13/2019 07:05:  Info AppDelegate.swift [33] dump(): Info message
05/13/2019 07:05:  Warn AppDelegate.swift [34] dump(): Warn message
05/13/2019 07:05: Error AppDelegate.swift [35] dump(): Error message
05/13/2019 07:05: Fatal AppDelegate.swift [36] dump(): Fatal message
HestiaTest.AppDelegate current level = warn
05/13/2019 07:05:  Warn AppDelegate.swift [44] dump(): Warn message
05/13/2019 07:05: Error AppDelegate.swift [45] dump(): Error message
05/13/2019 07:05: Fatal AppDelegate.swift [46] dump(): Fatal message
```

## Terminal based Applications

Terminal based apps are approached a little differently; frameworks are not
used but the various souce files are either copied into the project or referenced
by the project. These files are then built directly as part of the terminal based project.

Assume the following configuation file:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<?oxygen RNGSchema="hestia.rng" type="xml"?>
<?oxygen SCHSchema="hestia.sch"?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
    
    <appenders>
        
        <appender name="console" class="StdOutAppender">
            <layout class="PatternLayout">
                <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm:ss}: [%p] %F:%L: %m"/>
            </layout>
        </appender>

    </appenders>
    
    <loggers>
        
        <root>
            <level value="debug"/>
            <appender-ref ref="consoleXML"/>
        </root>
        
        <logger name="TerminalExample.console">
            <level value="debug"/>
            <appender-ref ref="console"/>
        </logger>

    </loggers>
    
</configuration>
```

The code for using the logger is relatively straight-forward, but there are a number of important
things to note. Firstly the program namespace, `programNamespace` is set
manually in a terminal based application.

```swift
import Foundation

LoggerFactory.programNamespace = "TerminalExample"
```

Declare an instance of the *LoggerFactory} and initialise it. Also declare the logger for this level of the program.

```swift
var loggerFactory: LoggerFactory = LoggerFactory.sharedInstance
var logger: Logger!
```

It is handy to encapsulate the initialisation process in a function.
This function can be used in Cocoa applications
with only minor changes to the path (taken from the resource bundle). The *logger* variable
is now linked to the *TerminalExample.console* logger declared in the configuration file.}

```swift
func setLoggingSystem() {
    do {
        let filePath = "<path to the configuration file to be used>"
        try loggerFactory.configure( from: "hestia.xml", inFolder: filePath )
        logger = try loggerFactory.getLogger( name: "TerminalExample.console" )
        logger.debug( "Logger initialised" )
    } catch let e as LoggerError {
        print( e.description )
    } catch {
        print( "Unknown error when setting logging system"  )
    }
}
```

Call the *setLoggingSystem* function to initialise everything.
swift
```
setLoggingSystem()
```

Also important, remember that the version number will not work in the terminal environment
so a suitable message is generated (see below).

```swift
logger.debug( "Version: \(LoggerFactory.version)" )
```

Output some example logging statements.}

```swift
print( "Current level = \(logger.getLevel() as LoggerLevelEnum)" )
logger.trace( "Trace message" )
logger.debug( "Debug message" )
logger.info( "Info message" )
logger.warn( "Warn message" )
logger.error( "Error message" )
logger.fatal( "Fatal message" )
```

Now reset the logger level from within the program to observe the changed threshold.

```swift
logger.set(level: "warn" )
print( "Current level = \(logger.getLevel() as LoggerLevelEnum)" )

logger.trace( "Trace message" )
logger.debug( "Debug message" )
logger.info( "Info message" )
logger.warn( "Warn message" )
logger.error( "Error message" )
logger.fatal( "Fatal message" )

try? logger.close()
```

The console output from the above would be:

```
03/13/2019 02:17:29: [Debug] main.swift:23: Logger initialised
03/13/2019 02:17:29: [Debug] main.swift:32: Version: Unknown version
   (terminal only apps do not support version number)
Current level = debug
03/13/2019 02:17:29: [Debug] main.swift:36: Debug message
03/13/2019 02:17:29: [ Info] main.swift:37: Info message
03/13/2019 02:17:29: [ Warn] main.swift:38: Warn message
03/13/2019 02:17:29: [Error] main.swift:39: Error message
03/13/2019 02:17:29: [Fatal] main.swift:40: Fatal message
Current level = warn
03/13/2019 02:17:29: [ Warn] main.swift:48: Warn message
03/13/2019 02:17:29: [Error] main.swift:49: Error message
03/13/2019 02:17:29: [Fatal] main.swift:50: Fatal message
Program ended with exit code: 0
```

## Installing

There are several downloadable files, not all of which are
needed depending on the type of application being developed:

* *Cocoa Applications*. This just requires the Framework that is in
the downloadable file `HestiaLogger.Framework.tar.gz`. After uncompressing
this file a framework file `HestiaLogger.Framework` is produced which
should be moved to either the user library folder `~/Library/Frameworks/`,
or the system folder `/Library/Frameworks/`.

* *Terminal Only Applications*. While it is possible to use the Hestia
framework with a terminal only program, it is not the most obvious and
straight-forward approach. A far easier method is to include the source
with your build. Although this means compiling the source within the app,
the overheads of this are very small. The download file is
`HestiaSource.tar.gz` which when expanded gives a folder containing
the raw source files that need to be added to the application build.




