# Using the Hestia Logger

A simple example to create a Cocoa based application.

## Overview

Let's create a simple Cocoa based example, *LoggerExample*. The following is based on extracts from 
a simple accounts application. First of all the logger is defined as a fileprivate variable specific to this file only
(s are all the other files that an application might use):

```swift
import Cocoa
import HestiaLogger

@main
class AppDelegate: NSObject, NSApplicationDelegate {
   
   fileprivate var logger: Logger!
```
Within the *applicationDidFinishLaunching* function the logger is initialised (if not already — unlikely). A simple
confirming debug message shows the logger version.
```swift
   func applicationDidFinishLaunching(_ aNotification: Notification)
   {
      if logger == nil {
         logger = LoggerFactory.setLoggingSystem( forClass: self.className )
      }
      logger.debug( "Initialised logger V\(LoggerFactory.version)" )
      // Remainder of the application set up
 
   }
```

Next an extension to the *LoggerFactory* class defines the *setLoggingSystem* function:

```swift
import Foundation
import HestiaLogger

extension LoggerFactory {

   /// Set up the logging for named class.
   ///
   /// - Parameters:
   ///   - forClass: name of class to use logger
   /// - Returns: A logger for this request.

   static func setLoggingSystem( forClass: String ) -> Logger? {

      var masterLoggerFactory: LoggerFactory!

      do {
         if masterLoggerFactory == nil {
            let embeddedConfigPath = Bundle.main.resourcePath!
            masterLoggerFactory = LoggerFactory.sharedInstance
            try masterLoggerFactory.configure( from: "hestia.xml", inFolder: embeddedConfigPath )
         }
         let logger = try masterLoggerFactory.getLogger( name: forClass )
         return logger
      } catch let e as LoggerError {
         Errors.displayFatalErrorAlert( e.description )
         return nil
      } catch {
         Errors.displayFatalErrorAlert( "Unknown error when setting logging system" )
         return nil
      }
   }
```
Finally set the logger configuration file, *hestia.xml*, which uses a simple *StdOutAppender* to the console. This file
is in the applications resource bundle folder.
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

   <appenders>
      <appender name="console" class="StdOutAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: [%p] %F:%M:%L: %m"/>
         </layout>
      </appender>
   </appenders>

   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>

      <logger name="HopVineAccounts.AppDelegate">
           <level value="debug"/>
           <appender-ref ref="console"/>
      </logger>
   </loggers>

</configuration>
```
The *HopVineAccounts.AppDelegate* uses a *debug* level logger.
All other classes within the application use the default *root* logger set to only output *info* messages.
If all works as intended the following *debug* message will be output:

```
11/15/2021 12:35: [Debug] AppDelegate.swift:applicationDidFinishLaunching(_:):60: Initialised logger V2.0b486
```

