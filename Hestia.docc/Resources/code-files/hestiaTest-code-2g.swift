import Foundation
import HestiaLogger

extension LoggerFactory {

   /// Set up the logging for named class.
   ///
   /// - Parameters:
   ///   - forClass: name of class to use logger
   ///   - file: name of file to use as configuraton
   /// - Returns: A logger for this request.

   static func setLoggingSystem( forClass: String, file: String = "hestia-configuration.xml" ) -> Logger? {

      var masterLoggerFactory: LoggerFactory!

      do {
         if masterLoggerFactory == nil {
            let embeddedConfigPath = Bundle.main.resourcePath!
            masterLoggerFactory = LoggerFactory.sharedInstance
            try masterLoggerFactory.configure( from: file, inFolder: embeddedConfigPath )
         }
         let logger = try masterLoggerFactory.getLogger( name: forClass )
         return logger
      } catch let e as LoggerError {
         print( e.description )
         return nil
      } catch {
         print( "Unknown error when setting logging system" )
         return nil
      }
   }

}
