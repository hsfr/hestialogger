import Foundation
import Cocoa
import HestiaLogger

extension ViewController {
      
   enum AppenderEnum: String {
      case consoleAppender = "console"
      case fileAppender = "file"
      case rollingFileAppender = "rollingFile"
      case windowAppender = "window"
      
      var description: String {
         switch self {
            case .consoleAppender:
               return "Console"
            case .fileAppender:
               return "File"
            case .rollingFileAppender:
               return "Rolling File"
            case .windowAppender:
               return "Window"
         }
      }
   }
   
}
