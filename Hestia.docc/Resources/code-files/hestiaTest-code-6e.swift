import Foundation
import HestiaLogger

class RootClass: NSObject {
   
   public var logger: Logger!
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
    init( configFileName: String )
   {
      super.init()
      logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
      logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
   }
   
   func generate() {
      logger.debug( "Start task: \(self.className) " )
      for i in 0..<3 {
         logger.debug( "loop: \(i)" )
      }
   }
   
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class Class_1: RootClass {
   
   override init( configFileName name: String )
   {
      super.init( configFileName: name )
      logger.info( "Starting" )
   }

}

class Class_2: RootClass {
   
   override init( configFileName name: String )
   {
      super.init( configFileName: name )
      logger.info( "Starting" )
   }

}

