import Cocoa
import HestiaLogger

@main
class AppDelegate: NSObject, NSApplicationDelegate {

   fileprivate var logger: Logger!

   func applicationDidFinishLaunching(_ aNotification: Notification) {
      if logger == nil {
         logger = LoggerFactory.setLoggingSystem( forClass: self.className )
      }
      logger.debug( "Initialised logger V\(LoggerFactory.version)" )
  }

   func applicationWillTerminate(_ aNotification: Notification) {
      // Insert code here to tear down your application
   }

   func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
      return true
   }

}
