@IBAction func testAction(_ sender: Any)
{
   let configFileName = "config-\(whichLayout.rawValue)-\(whichAppender.rawValue).xml"
   
   let class_1 = Class_1( configFileName: configFileName )
   let class_2 = Class_2( configFileName: configFileName )
   let class_3 = Class_3( configFileName: configFileName )
   
   if isConcurrent {
      DispatchQueue( label: "HestiaTest-1.queue", attributes: .concurrent ).async {
         class_1.logger.debug( "Start task: Class_1" )
         class_1.generate()
      }
      
      DispatchQueue( label: "HestiaTest-2.queue", attributes: .concurrent ).async {
         class_2.logger.debug( "Start task: Class_2" )
         class_2.generate()
      }
      
      DispatchQueue( label: "HestiaTest-3.queue", attributes: .concurrent ).async {
         class_3.logger.debug( "Start task: Class_3" )
         class_3.generate()
         try? class_3.logger.close()
      }
   } else {
      class_1.generate()
      class_2.generate()
      class_3.generate()
      try? class_3.logger.close()
   }
}
