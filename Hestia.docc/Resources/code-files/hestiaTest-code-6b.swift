import Foundation
import HestiaLogger

class RootClass: NSObject {
   
   public var logger: Logger!
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
    init( configFileName: String )
   {
      super.init()
      logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
      logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
   }
   
}
