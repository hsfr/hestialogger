override func viewDidLoad() {
   super.viewDidLoad()
   if logger == nil {
      logger = LoggerFactory.setLoggingSystem( forClass: self.className )
   }
   logger.info( "View has loaded." )
}
