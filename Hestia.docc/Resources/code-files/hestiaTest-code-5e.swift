import Cocoa
import HestiaLogger

class ViewController: NSViewController {
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   fileprivate var logger: Logger!

   var whichAppender = AppenderEnum.consoleAppender

   @IBOutlet weak var consoleAppenderButton: NSButton!
   @IBOutlet weak var fileAppenderButton: NSButton!
   @IBOutlet weak var rollingFileAppenderButton: NSButton!
   @IBOutlet weak var windowAppenderButton: NSButton!

   var whichLayout = LayoutEnum.simpleLayout
   
   @IBOutlet weak var simpleLayoutButton: NSButton!
   @IBOutlet weak var patternLayout: NSButton!
   @IBOutlet weak var xmlLayout: NSButton!

   var isConcurrent = false

   @IBOutlet weak var concurrentCheckBox: NSButton!
