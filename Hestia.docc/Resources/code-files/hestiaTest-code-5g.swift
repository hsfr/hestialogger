override func viewDidLoad() {
   super.viewDidLoad()
   if logger == nil {
      logger = LoggerFactory.setLoggingSystem( forClass: self.className )
   }
   logger.info( "View has loaded." )
   consoleAppenderButton.state = NSControl.StateValue.on
   simpleLayoutButton.state = NSControl.StateValue.on
   concurrentCheckBox.state = NSControl.StateValue.off
}
