import Cocoa

class ViewController: NSViewController {

   fileprivate var logger: Logger!

   override func viewDidLoad() {
      super.viewDidLoad()
      if logger == nil {
         logger = LoggerFactory.setLoggingSystem( forClass: self.className )
      }
      logger.info( "View has loaded." )
   }

   override var representedObject: Any? {
      didSet {
      // Update the view, if already loaded.
      }
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func testAction(_ sender: Any)
   {
   }

}
