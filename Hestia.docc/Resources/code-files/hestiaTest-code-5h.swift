// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

@IBAction func appenderChanged( _ sender: AnyObject ) {
   let theAppender = sender as! NSButton
   whichAppender = resolveAppenderWithTitle( theAppender.title )
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

@IBAction func layoutChanged( _ sender: AnyObject ) {
   let theLayout = sender as! NSButton
   whichLayout = resolveLayoutWithTitle( theLayout.title )
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

@IBAction func concurrentAction( _ sender: NSButton ) {
   isConcurrent = ( sender.state == .on )
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

@IBAction func testAction(_ sender: Any)
{
}

