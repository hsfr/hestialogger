import Cocoa
import HestiaLogger

class ViewController: NSViewController {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   fileprivate var logger: Logger!

   @IBOutlet weak var consoleAppenderButton: NSButton!
   @IBOutlet weak var fileAppenderButton: NSButton!
   @IBOutlet weak var rollingFileAppenderButton: NSButton!
   @IBOutlet weak var windowAppenderButton: NSButton!

   @IBOutlet weak var simpleLayoutButton: NSButton!
   @IBOutlet weak var patternLayout: NSButton!
   @IBOutlet weak var xmlLayout: NSButton!

   @IBOutlet weak var concurrentCheckBox: NSButton!
