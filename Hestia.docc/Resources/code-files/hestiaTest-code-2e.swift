import Foundation
import HestiaLogger

extension LoggerFactory {

   /// Set up the logging for named class.
   ///
   /// - Parameters:
   ///   - forClass: name of class to use logger
   ///   - file: name of file to use as configuraton
   /// - Returns: A logger for this request.

   static func setLoggingSystem( forClass: String, file: String = "hestia-configuration.xml" ) -> Logger? {

   }

}
