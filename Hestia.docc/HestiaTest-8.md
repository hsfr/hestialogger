# Generating a Configuration File

Each of the tests requires a separate configuration file that defines the logging appander and layout. Only
one of the configuration files is presented, the remaining files can be found in the download [here]().

> Note: The XML name space is not specifically
required except if a schema is used to verify the configuration.

Create a simple XML file with blank appenders and loggers tag.
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

   <appenders>
   </appenders>

   <loggers>
   </loggers>

</configuration>
```
}

This is a configuration that is to support a _LogWindow_ with a _PatternLayout_ so add the 
appender definition which is names _logWindow_.
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

   <appenders>
      <appender name="logWindow" class="WindowAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
      </appenders>

   <loggers>
   </loggers>

</configuration>
```
Next the toot logger which is the default if no further loggers are defined.
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

   <appenders>
      <appender name="logWindow" class="WindowAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>

   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
   </loggers>

</configuration>
```
Finally define a set of loggers for each of the 
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

   <appenders>
        <appender name="logWindow" class="WindowAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>

   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>

      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="logWindow"/>
      </logger>

      <logger name="HestiaTest.ViewController">
         <level value="debug"/>
         <appender-ref ref="logWindow"/>
      </logger>

      <logger name="HestiaTest.Class_1">
         <level value="info"/>
         <appender-ref ref="logWindow"/>
      </logger>

      <logger name="HestiaTest.Class_2">
         <level value="info"/>
         <appender-ref ref="logWindow"/>
      </logger>

      <logger name="HestiaTest.Class_3">
         <level value="info"/>
         <appender-ref ref="logWindow"/>
      </logger>
   </loggers>

</configuration>
```

