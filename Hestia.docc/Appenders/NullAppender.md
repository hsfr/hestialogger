# Null Appender

Does nothing and "eats" the log message.

## Overview

Use this appender if temporary inhibiting of output messages is required. It is defined using

```xml
<appender name="null" class="NullAppender"/>
```

In most cases it is better to set the level to `off`. For example if there is a configuration file
definition

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <appender name="null" class="NullAppender"/>

      <appender name="console" class="WindowAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
 ```
Then the following logger definitions are identical in their effect.

```xml
      <logger name="HestiaTest.AppDelegate">
         <level value="off"/>
         <appender-ref ref="logWindow"/>
      </logger>

      <logger name="HestiaTest.AppDelegate">
         <level value="console"/>
         <appender-ref ref="null"/>
      </logger>

   </loggers>
   
</configuration>
```
The advantage of using the level set to `off` is when the _WindowAppender_ is used. The level menu
can be adjusted to increase the level, which cannot be done with _NullAppender_.

