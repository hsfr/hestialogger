# Window Appender

The WindowAppender is designed to support an in-application logging window.

![Window Appender Output.](block-diagram-windowAppender.png)

The `WindowAppender` is designed to be used either within Xcode or from an application running
outside the development environment. The benefits of using it are several: colour coding the output
so that different loggers can be distinguished; the log levels can be changed dynamically; and the
loggers can be selected as required.

> Note:  Using the `WindowAppender` with <doc:XmlLayout> is not advised as the results will only be confusing. 

> Warning:  Note that there are some restrictions, the chief of which is it 
cannot be accessed from background tasks. 

The ` WindowAppender` is defined in a similar fashion to <doc:StdOutAppender>:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
        <appender name="logWindow" class="WindowAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="logWindow"/>
      </logger>
      
      ...
   </loggers>
   
</configuration>
```

The log window shows up to 12 entry slots which can be made to display messages from a named class.

![Window Appender Details](image-windowAppender-1.png)

When the log window is first displayed the values of the Logger (class name) and the levels are preset to the
values in the configuration file. Thereafter these can be changed as the user requires. The colours of the 
log messages are preset andd cannot be changed.

The advantage of colourising the messages makes it easuer to read complex error mesage where the latter
are very similar between classes. Experience has shown this to be a major adantage, especially for concurrent
tasks where the error messages are mixed.

there are two controls: saving the window contents to a file (no colour), and clearing the text area.

It is also possible to declare several appenders for each logger. See <doc:FileAppender>.

