# Rolling File Appender

Writes output to a file series specifying a maximum size and number of files.

![Rolling File Appender Output.](block-diagram-rollingFileAppender.png)

When the base file reaches a fixed maximum size it is pushed onto a rolling stack of
files that are indexed according a choice defined in the configuration.

```xml
<appender name="rollingFile" class="RollingFileAppender">
  <param name="append" value="false"/>
  <param name="file" value="/Users/hsfr/Library/Logs/HestiaTest/hestia.log"/>
  <param name="maxFileSize" value="200"/>
  <param name="maxBackupIndex" value="10"/>
  <param name="nameSuffix" value="true"/>
  <layout class="PatternLayout">
     <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss}: [%p] %F:%L: %m"/>
  </layout>
</appender>
```
The appender definition uses five parameters

- term _append_: Determines whether the file is added to (_true_) or restarted when the application is rerun (_false_).
- term _file_: An absolute path definition of the log file.
- term _maxFileSize_: The maximum size in bytes that the base log file should not exceed.
- term _maxBackupIndex_: The maximum number of overspill files tht are permitted.
- term _nameSuffix_: The style of the index addition. If true then the style of the file name will be 
_hestia-n.log_, if _false_ the it will be _hestia.log.n_, where _n_ is an integer between 1 and _maxBackupIndex_.

Each appender definition must have an associated layout defined. To clarify when the base log file (_hestia.log_) reaches 
_maxFileSize_ it is copied to the next file in the sequence (_hestia-1.log_). If there is a file _hestia-1.log_ then that is moved to
_hestia-2.log_ and so on, The diagram below should make this clear. 

![Rolling File Growth.](block-diagram-rollingFileAppender-folders-1.png)

The final file (_hestia-maxBackupIndex.log_ or _hestia.log.maxBackupIndex_) will discarded before being written to.

> Note: If the _append_ flag is set to _false_ then the log file defined by the
_file_ deleted together with all the related overspill files. If _nameSuffix_ is set to _false_
all files matching the pattern `hestia.log` and `hestia.log.*` will be deleted.

### Using the RollingFileAppender with XmlLayout

The same caveats to using `RollingFileAppender` with the <doc:FileAppender> as with the <doc:XmlLayout>.
An XML header is written out first to satisfy
the requirements for well-formedness.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="hestiaXmlOutput.rng" type="xml"?>
<logOutput xmlns="http://www.hsfr.org.uk/Schema/Hestia">
```
Each log file in  the sequence will naturally have a start tag inserted when a new base file is created. It will also have an
end tag automatically aded when the base fie is copied to the first in the sequence. This means that each
file in the sequence is well formed. The thing to note hear is that the file jist cannot be concatenated together to
form a single well-formed XML file.

It is still the responsibilty of the application designer to close the base file corretly with a closing 
tag before the application is closed.

It is also possible to declare several appenders for each logger. See <doc:FileAppender>.
