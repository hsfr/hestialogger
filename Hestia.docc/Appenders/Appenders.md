# Appenders

Each logger has one or more attached appender that are used to write the log message to the
appropriate destination, file or console, in a variety of formats. They are identified 
by a unique string in the configuration file.


## Overview

The general format of an appender definition is:

```xml
<appender name="unique_name_of_appender" class="type_of_appender">
   <!-- list of optional parameters -->
   <!-- Layout definition -->
</appender>
```
where the type of the appender can one of the appenders below.

## Topics

### Essentials

- <doc:NullAppender>
- <doc:StdOutAppender>
- <doc:FileAppender>
- <doc:RollingFileAppender>
- <doc:WindowAppender>

