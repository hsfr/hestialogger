# File Appender

Writes output to a single named file.

![File Appender Output.](block-diagram-fileAppender.png)

In cases where a permanent record needs to be taken for later inspection of the logs is required
it is possible to write to a named file using the _FileAppender_. To define a _FileAppender_
use the following

```xml
<appender name="file" class="FileAppender">
  <param name="append" value="false"/>
  <param name="file" value="/Users/hsfr/Library/Logs/HestiaTest/hestia.log"/>
  <layout class="PatternLayout">
     <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
  </layout>
</appender>
```
The appender definition uses two parameters

- term _append_: Determines whether the file is added to (_true_) or restarted when the application is rerun (_false_).
- term _file_: An absolute path definition of the log file.

Each appender definition must have an associated layout defined. For example it might be used as

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
       <appender name="file" class="FileAppender">
         <param name="append" value="false"/>
         <param name="file" value="/Users/hsfr/Library/Logs/HestiaTest/hestia.log"/>
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="file"/>
      </logger>
   </loggers>
   
</configuration>
```

Similar to `ConsoleAppender` for example, assuming that the logger has been configured with a file
_configFileName_ set to '`config-pattern-file.xml`':

```swift
logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
```

would output the following to the log file.

```
02/12/2021 12:09:00 : [Debug] Classes.swift:22: Root class HestiaTest.FirstClass has loaded logger with config-pattern-file.xml
```

Note that if the *XmlLayout* format is used an XML header is written out first to satisfy
the requirements for well-formedness.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="hestiaXmlOutput.rng" type="xml"?>
<logOutput xmlns="http://www.hsfr.org.uk/Schema/Hestia">
```

The RNG schema reference is merely a convenience and is not really necessary. This also requires that the logger is closed to
ensure an XML footer (closing tag) is output; this is the responsibility of the 
application designer. It can be closed  using the following
```swift
try? className.logger.close()
```
writing the following to the file

```xml
</logOutput>
```

It is also possible to declare several appenders for each logger. The following would allow mnitoring of the messages
on the console as well as writing them to a file.


```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <appender name="console" class="StdOutAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
      <appender name="file" class="FileAppender">
         <param name="append" value="false"/>
         <param name="file" value="/Users/hsfr/Library/Logs/HestiaTest/hestia.log"/>
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="console"/>
         <appender-ref ref="file"/>
      </logger>
   </loggers>
   
</configuration>
```
