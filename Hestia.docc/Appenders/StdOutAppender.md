# Console Appender

Outputs the log message to the Xcode console (StdOut).

![File Appender Output.](block-diagram-stdoutAppender.png)

This is only only suitable for debugging in Xcode or from within a terminal only application. A typical
use would define a pattern layout:

```xml
<appender name="console" class="StdOutAppender">
   <layout class="PatternLayout">
      <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m"/>
   </layout>
</appender>
```
The reference name of the appender, *console*, is used to reference the appender in the logger definition. The class
*StdOutAppender* defines te appender. Each appender definition must have an associated layout defined.
A typical use would be 

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <appender name="console" class="StdOutAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="console"/>
      </logger>
      
    </loggers>
   
</configuration>
```

For example, assuming that the logger has been configured with a file
_configFileName_ set to '`config-pattern-console.xml`':

```swift
logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
```
would output the following to the Xcode console
```
02/12/2021 12:09:00 : [Debug] Classes.swift:22: Root class HestiaTest.FirstClass has loaded logger with config-pattern-console.xml
```

