# Configuration

The heart of describing the logging process is the Configuration File. 
This is an XML formatted file that must be loaded before 
the logging system is used.

## Overview

The overall structure of the config file is:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <!-- List of appenders -->
   </appenders>
   
   <loggers>
      <!-- root logger definition -->
      <!-- List of loggers -->
   </loggers>
   
</configuration>
```

## Configuration Format

The format of the logger configuration file 
is virtually identical to that used by *log4java*. There are two schema files 
supplied with the framework and source code, `hestia.rng` and `hestia.sch` 
which can be used to check the configuration files. Note that the Schematron 
file is extensible and currently only represents a bare minimum.

If the schemas are used within a suitable editor, such as  [oXygen](https://www.oxygenxml.com), 
then the configuration file should be changed to the following or
equivalent to support your editor of choice:

```xml
<?xml version="1.0" encoding="UTF-8" ?>

<?oxygen RNGSchema="hestia.rng" type="xml"?>
<?oxygen SCHSchema="hestia.sch"?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

   <appenders>
      <!-- List of appenders -->
   </appenders>

   <loggers>
      <!-- root logger definition -->
      <!-- List of loggers -->
   </loggers>

</configuration>
```

For further details if defining the configuration file see the indovidual articles.

- <doc:Formatting>
- <doc:Appenders>

