# ``HestiaLogger``

The Hestia Logger is a light-weight logging framework for Swift 4 similar to
[log4swift](https://github.com/jduquennoy/Log4swift) (Hestia uses a similar XML
configuration file). It supports both Cocoa based
and terminal based applications. The latter types do not use Frameworks and so
require a slightly modified way of including the logger.

Hestia has a reasonable set of features which make logging simple to use over a range of Cocoa and
terminal based applications:

* Easy to use for the simplest cases.
* Works in a terminal only environment.
* Logging to Xcode console.
* Logging to window within application (only non-terminal apps)
* Output to XML format file.
* Rolling file output.
* Setting and tesing log levels in code.
* Limited in-code reconfiguration.
* Uses same XML format configuration as *log4j*.
* Schema support for configuration file using RELAX-NG and Schematron.

It does not support the following:

* Network Logging.
* Asynchronous logging.
* Auto-reload of configuration.
* Logging parameters using closures.

It is intended that some of the above will be added in future versions, if there is a need.

> Note: Hestia is
only designed to be used in a Mac OS-X environment not iOS.

The current version is available either from the [Hestia web site](http://www.hestia.hsfr.org.uk/home.html)
or the BitBucket [Repository](https://bitbucket.org/hsfr/hestialogger/src/master/).

## Topics

### Essentials

- <doc:Concepts>
- <doc:UsingHestia>
- <doc:Configuration>
- <doc:Formatting>
- <doc:Appenders>

### Creating Loggers

- ``LoggerFactory`` 
