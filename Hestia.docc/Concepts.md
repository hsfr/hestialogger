# Logging Concepts

Applications use the logging system by declaring an instance 
of a logger and defining the look and destination of the logging 
message (in a configuration file). Whether a message is output
is dependent on the *level* of the log. 

## Overview

### Levels

Levels determine whether a particular logging statement is successful.
There are 7 levels (identical to *log4j*):

*  trace = 0
*  debug = 1
*  info = 2
*  warn = 3
*  error = 4
*  fatal = 5
*  off = 6

To put in another form

```
trace < debug < info < warn < error < fatal < off
```
The logger level determines that all logging statements which have an
associated value less than the level will not be output. For example
if a logger's level is defined as *warn* then a *logger.info()*
statement will be ignored. The *off* level is a convenience to
inhibit a logger either from the configuration
file or set in the code.

### Logging Statements

Assuming a logger has been defined to output *debug* messages to the console
```xml
<logger name="class_name">
   <level value="debug"/>
   <appender-ref ref="console"/>
</logger>
```
In the class _class_name_ it is possible to say
```
logger.debug( "Debug message" )
```
which will output debug messages to the Xcode console. Any statements
```
logger.info( "Info message" )
```
will be output also. However if the logging level for that logger is set to _info_
in the configuration file then the _debug_ level logger messages will not be output.

Note that the <doc:WindowAppender> allows changing the logger level as the application is run
rather than having to change the configuration file.

## Changing/Inspecting Levels

It is possible modify and inspect the current levels from the application. The latter is useful
if a log statement is dependant on a block of code that is only used for the logger. In
that case the entre code block can be avoided if not required.

- ``Logger/getLevel()``
- ``Logger/check(level:)``
- ``Logger/set(level:)``
