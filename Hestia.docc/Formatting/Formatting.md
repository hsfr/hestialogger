# Formatters

Formatters are used to format the output of the logger.

## Overview

Currently there are 3 formatters:

* SimpleLayout
* PatternLayout
* XmlLayout

## Topics

### Essentials

- <doc:SimpleLayout>
- <doc:PatternLayout>
- <doc:XmlLayout>


