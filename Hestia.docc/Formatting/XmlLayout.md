# XML Layout

Outputs the log message formatted as a XML scrap.

![XML Layout Output.](block-diagram-xmlLayout.png)

When further processing of the logs is required it is
possible to output the log messages in an XML format

The layout is used by the appender to format 
the output message. It is defined in the configuration file, for example
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <appender name="console" class="StdOutAppender">
         <layout class="XmlLayout">
            <param name="ConversionPattern" value="%d{dd/MM/yyyy hh:mm:ss}"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="console"/>
      </logger>
   </loggers>
   
</configuration>
```

The `ConversionPatter` defines how the date is output. Any other details (such as `%m`, etc.) will be ignored.
For example, assuming that the logger has been configured with a file
_configFileName_ set to '`config-xml-console.xml.log`':

```swift
logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
```
would give
```xml
<log-event date="02/12/2021 05:16:38">
   <data name="Classes.swift" line="22" function="Classes.swift" level="Debug" />
   <message>Root class HestiaTest.FirstClass has loaded logger with config-xml-console.xml.xml</message>
</log-event>
```

> Warning: There are important caveats here: the output is in the form of a set of single XML tagged statements.
*It is not a well-formed XML document.*

In order to construct a well-formed document
an opening a closing tag must surround the output. When an appender is started,
either to a file or the console, the logger checks to see whether this is the first time it has been written to (when not
appending to the logger). If it is the first write then an opening tag is writing to the logger:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="hestiaXmlOutput.rng" type="xml"?>
<logOutput xmlns="http://www.hsfr.org.uk/Schema/Hestia">
```

The RNG schema reference is merely a convenience and is not really necessary. When any of the files or console
output are closed (implicitly through application exit) the following closing tag is output:

```xml
</logOutput>
```

