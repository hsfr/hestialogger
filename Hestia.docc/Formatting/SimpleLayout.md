# Simple Layout

The simplest output layout.

![Simple Layout Output.](block-diagram-simpleLayout.png)

No information is given from where the logger
has been called, use the <doc:PatternLayout> formatter if this is required.
The layout is used by the appender to format the output message.
It is defined in the configuration file, for example
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <appender name="console" class="StdOutAppender">
         <layout class="SimpleLayout"/>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="console"/>
      </logger>
   </loggers>
   
</configuration>
```

The error messages are formatted in the form "`level - message`".

For example, assuming that the logger has been configured with a file
_configFileName_ set to '`config-simple-console.xml`':

```swift
logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
```
would give
```
debug - Root class HestiaTest.AppDelegate has loaded logger with config-simple-console.xml
```

