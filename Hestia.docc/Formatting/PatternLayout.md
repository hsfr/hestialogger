# Pattern Layout

The commonest layout format that gives substantially more
information which is selected by the configuration file definition.

![Pattern Layout Output.](block-diagram-patternLayout.png)

Most of the time log messages require more information to link with the
source code that called the logger. Usually at mimimum this is the file,
line number and log level.
If this information is not required use the <doc:SimpleLayout> formatter.

The layout is used by the appender to format 
the output message. It is defined in the configuration file, for example
```xml
<?xml version="1.0" encoding="UTF-8" ?>

<configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">
   
   <appenders>
      <appender name="console" class="StdOutAppender">
         <layout class="PatternLayout">
            <param name="ConversionPattern"
                   value="%d{dd/MM/yyyy hh:mm:ss} : [%p] %F:%L: %m"/>
         </layout>
      </appender>
   </appenders>
   
   <loggers>
      <root>
         <level value="info"/>
         <appender-ref ref="console"/>
      </root>
      
      <logger name="HestiaTest.AppDelegate">
         <level value="debug"/>
         <appender-ref ref="console"/>
      </logger>
   </loggers>
   
</configuration>
```
which defines a preset named pattern `ConversionPattern` with a pattern format where
```
%d{} - formatted date
%F   - file name containing the log statement
%M   - function name where log statement occurs
%p   - log level
%L   - line number within file where log statement occurs
%m   - log message
```

For example, assuming that the logger has been configured with a file
_configFileName_ set to '`config-pattern-console.xml`':

```swift
logger = LoggerFactory.setLoggingSystem( forClass: self.className, file: configFileName )
logger.debug( "Root class \(self.className) has loaded logger with \(configFileName)" )
```
would give
```
02/12/2021 12:09:00 : [Debug] Classes.swift:22: Root class HestiaTest.FirstClass has loaded logger with config-pattern-console.xml
```
