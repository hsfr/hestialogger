//
//  LoggerFactory+Utilities.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 28/05/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - LoggerFactory Class
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/**
 # LoggerFactory Utilities Properties
*/

extension LoggerFactory {

   /// List of logger names from the configuration file.
   ///
   /// No maximum limit.
   static var loggerNamesList = [String]()

   /// List of levels used for each logger to preset the windowAppender levels.
   ///
   /// No maximum limit keyed with name of logger
   static var loggerInitialLevelList = [String: String]()

   /// List of appenders used for each logger to preset the windowAppender levels.
   ///
   /// No maximum limit.
   // static var appenderNamesList = [String: String]()

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - General Utility Functions
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Get the formatted current data string.
   ///
   /// - Parameters:
   ///   - format: The date format. For example "`MM/dd/yyyy hh:mm`"
   ///
   /// - Returns: The current formatted date

   static func getCurrentDateString( format: String ) -> String
   {
      let date = Date()
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = format
      return dateFormatter.string( from: date )
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Does the file exist?
   //
   // Done like this in case other checks need to be done.
   ///
   /// - parameter file: the file name and path
   /// - returns: true if a file at the specified path/filename exists, otherwise false

   static func fileExists( _ file: String ) -> Bool
   {
      return FileManager().fileExists( atPath: file )
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Does the file not exist?
   //
   // Done like this in case other checks need to be done.
   ///
   /// - parameter file: the file name and path
   /// - returns: true if a file at the specified path/filename does not exist, otherwise false

   static func fileDoesNotExist( _ file: String ) -> Bool
   {
      return !fileExists( file )
   }

}
