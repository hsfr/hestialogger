//
//  LoggerFactory+XML.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 28/05/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

extension LoggerFactory {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Generalised XML tag output with attributes.
   ///
   /// For example:
   ///
   /// ```
   /// print( LoggerFactory.outputTagWithAttributes( APPENDER_REF_TAG, attrib: ( APPENDER_REF_ATTRIB, appenderRef.ref ) ) )
   /// ```
   /// would output (for example)
   ///
   /// ```
   /// <appender-ref ref="console"/>
   /// ```
   ///
   /// - Parameters:
   ///   - tag: the XML tag name
   ///   - attrib: variadic number of attribute pairs (name,value)
   ///   - contents: optional contents string
   /// - Returns: XML string

   static func outputTagWithAttributes( _ tag: String, attrib: ( String, String )..., contents c: String = "" ) -> String
   {
      var result = "<\(tag)"
      for ( name, value ) in attrib {
         result += " \(name)=\"\(value)\""
      }
      if c.isEmpty {
         result += " />\n"
      } else {
         result += ">\(c)</\(tag)>\n"
      }
      return result
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Generalised XML tag output without attributes.
   ///
   /// For example:
   ///
   /// ````
   /// print( outputTag( "text", contents: "some text" ) )
   /// ````
   /// would output (for example)
   ///
   /// ````
   /// <text>some text</text>
   /// ````
   ///
   /// - Parameters:
   ///   - name: the XML tag name
   ///   - contents: optional contents string
   /// - Returns: XML string

   static func outputTag( _ tag: String, contents c: String = "" ) -> String
   {
      var result = "<\(tag)"
      if c.isEmpty {
         result += "/>"
      } else {
         result += ">\(c)</\(tag)>"
      }
      return result
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   var outputXML: String {
      var result = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>"
      result += "<?oxygen RNGSchema=\"hestia.rng\" type=\"xml\"?>"
      result += "<?oxygen SCHSchema=\"hestia.rng\"?>"

      var appendersXML = ""
      for ( _, appender ) in appenders.appendersList {
         appendersXML += appender.outputXML
      }
      var loggersXML = ""
      for ( _, logger ) in loggers.loggersList {
         loggersXML += logger.outputXML
      }
      var contents = LoggerFactory.outputTag( APPENDERS_TAG, contents: appendersXML )
      contents += LoggerFactory.outputTag( LOGGERS_TAG, contents: loggersXML )

      result += LoggerFactory.outputTagWithAttributes( CONFIGURATION_TAG, attrib: ( "xmlns", "http://www.hsfr.org.uk/Schema/Hestia" ), contents: contents )
      return result
   }

}


