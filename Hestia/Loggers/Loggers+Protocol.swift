//
//  Loggers+Protocol.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 01/06/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

typealias loggersListType = [ String: LoggerProtocol ]

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// The LoggersProtocol defines a list of Loggers.

protocol LoggersProtocol {

   /// List of loggers keyed by name
   var loggersList: loggersListType { get set }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Does the logger exist?
   ///
   /// - Parameters:
   ///   - name: The name of the logger to find
   /// - Returns: true if exists

   func loggerExists( _ name: String ) -> Bool

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Find the logger in the list. If it is not in the list
   /// it throws an error.
   ///
   /// - Parameters:
   ///   - name: The name of the logger to find
   /// - Throws: LoggerError.cannotFindLogger

   func findLogger( _ name: String ) throws -> LoggerProtocol
}

