//
//  Logger+Protocol.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 01/06/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Enumeration Types
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// There are two different types of logger: a root one which
/// is the default to use if the required logger is not found,
/// everything else is a defined logger.

enum LoggerType: Int {
   case root
   case userDefined
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

protocol LoggerProtocol {

   /// The name of the appender referenced by the logger
   var name: String { get set }

   /// The type (root or normal logger) of this logger
   var type: LoggerType { get set }

   /// The level that this logger will be activated
   var level: LevelProtocol { get set }

   /// A list of appenders associated with this logger. A list of names of the
   /// appenders.
   /// var appenderRefList: [ String: AppenderRefProtocol ] { get set }

   /// The list of required appenders keyed by name.
   var appenderList: AppenderListType { get set }

   /// Is this the first write to the selected logger (for appending)
   var firstWrite: Bool { get set }

   var description: String { get }

   /// The primary entry into the selected appender. Note that
   /// this should never be called by the user (internal method)
   ///
   /// - Parameters:
   ///   - name: The name of this logger
   ///   - logLevel: The requested log level for this log message
   ///   - message: The log message
   ///   - file: The file which originated the log message
   ///   - line: The line where the log message is called
   ///   - function: The function in which the log message is called

   func writeLog( logLevel: LoggerLevelEnum,
                  message: String,
                  file: StaticString,
                  line: Int,
                  function: StaticString )

   /// Close this logger. Only relevent with an XML based logger
   /// since its sole task is to write the XML footer to the file.

   func close() throws

   /// Set the logging level dynamically.
   ///
   /// - Parameters:
   ///   - level: The logging level as a string "info", "debug" etc

   func set( level: String )

   /// Get the current logging level.
   ///
   /// - Returns: the current logging level as LoggerLevel.

   func getLevel() -> LoggerLevelEnum

   /// Check the current logging level.
   ///
   /// - Parameters:
   ///   - level: The logging level to check against
   /// - Returns: true if the current logging level is less than or equal to the input level

   func check( level: LoggerLevelEnum ) -> Bool

   /// Log messages. Note that only the first parameter is used.
   ///
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   func trace( _ msg: String, file: StaticString, line: Int, function: StaticString )
   func debug( _ msg: String, file: StaticString, line: Int, function: StaticString )
   func info( _ msg: String, file: StaticString, line: Int, function: StaticString )
   func warn( _ msg: String, file: StaticString, line: Int, function: StaticString )
   func error( _ msg: String, file: StaticString, line: Int, function: StaticString )
   func fatal( _ msg: String, file: StaticString, line: Int, function: StaticString )
   func off( _ msg: String, file: StaticString, line: Int, function: StaticString )
}

