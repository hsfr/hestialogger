//
//  Logger.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 04/03/2019.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Class Definitions
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// The Logger class contains the details of the particular
/// logger defined in the configuration file.
///
/// Application code writes the log using a logger which is the
/// primary means to generate logs. Each logger has an associated
/// level which controls whether that log succeeds. Loggers are
/// generally identified by a simple name, but it is suggested
/// that the name of the class in which they are declared is used.
/// If the named logger cannot be found a generic “root” logger
/// is used (and therefore must be defined in the configuration file).


public class Logger: LoggerProtocol {

   /// The name of the logger
   var name: String

   /// The type (root or normal logger) of this logger
   var type: LoggerType

   /// The level that this logger will be activated
   var level: LevelProtocol

   /// The reference to the required appender
   /// var appenderRefList: [ String: AppenderRefProtocol ]

   /// The list of required appenders keyed by name
   var appenderList: AppenderListType

   /// Is this the first write to the selected logger (for appending)
   internal var firstWrite: Bool

   var description: String {
      var msg = "LoggerName: \(name), "
      msg += "level:\(level.enumValue), "
      msg += "firstWrite:\(firstWrite)"
      return msg
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Initialise instance of logger with name.
   ///
   /// - Parameters:
   ///   - identifier: Name of logger to create

   init( identifier: String ) {
      type = .userDefined
      name = identifier
      level = Level()
      appenderList = AppenderListType()
      // Only time this is set to true when a new log is set up
      firstWrite = true
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Destroy instance of logger.

   deinit {
      // try? self.close()
   }

   /// Log trace level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple
   /// ```
   ///    logger.trace( "Trace level message" )
   /// ```
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func trace( _ msg: String = "",
                      file: StaticString = #file,
                      line: Int = #line,
                      function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.trace, message: msg, file: file, line: line, function: function )
   }

   /// Log debug level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple
   /// ```
   ///    logger.debug( "Debug level message" )
   /// ```
   /// - Parameters:
   ///   - msg: Message to output
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func debug( _ msg: String = "",
                      file: StaticString = #file,
                      line: Int = #line,
                      function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.debug, message: msg, file: file, line: line, function: function )
   }

   /// Log info level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple
   /// ````
   ///    logger.info( "Info level message" )
   /// ````
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func info( _ msg: String = "",
                     file: StaticString = #file,
                     line: Int = #line,
                     function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.info, message: msg, file: file, line: line, function: function )
   }

   /// Log warn level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple
   /// ```
   ///    logger.warn( "Warn level message" )
   /// ```
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func warn( _ msg: String = "",
                     file: StaticString = #file,
                     line: Int = #line,
                     function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.warn, message: msg, file: file, line: line, function: function )
   }

   /// Log error level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple
   /// ```
   ///    logger.error( "Error level message" )
   /// ```
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func error( _ msg: String = "",
                      file: StaticString = #file,
                      line: Int = #line,
                      function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.error, message: msg, file: file, line: line, function: function )
   }

   /// Log fatal level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple
   /// ```
   ///    logger.fatal( "Fatal level message" )
   /// ```
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func fatal( _ msg: String = "",
                      file: StaticString = #file,
                      line: Int = #line,
                      function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.fatal, message: msg, file: file, line: line, function: function )
   }

   /// Log off level messages.
   ///
   /// Note that only the first parameter is used.
   /// Calling this function is simple (and should not appear)
   /// ```
   ///    logger.off( "Off level message" )
   /// ```
   /// - Parameters:
   ///   - msg: Message to output (default to null string)
   ///   - file: File from which logger is called (default only)
   ///   - line: Line in file from which logger is called (default only)
   ///   - function: Functio/method in file from which logger is called (default only)

   public func off( _ msg: String = "",
                    file: StaticString = #file,
                    line: Int = #line,
                    function: StaticString = #function ) {
      writeLog( logLevel: LoggerLevelEnum.off, message: msg, file: file, line: line, function: function )
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Close this logger.
   ///
   /// - Throws: LoggerError if cannot close log file

   public func close() throws {
      // Go round all attached appenders and close them if necessary
      for ( _, appender ) in appenderList {
         do {
            try appender.close()
         } catch let error as LoggerError {
            throw error
         } catch {
            throw LoggerError.init( kind: ErrorKind.unknownError )
         }
      }
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// The primary entry into the selected appender.
   ///
   /// - Parameters:
   ///   - logLevel: The requested log level for this log message
   ///   - message: The log message
   ///   - file: The file which originated the log message
   ///   - line: The line where the log message is called
   ///   - function: The function in which the log message is called

   internal func writeLog( logLevel: LoggerLevelEnum,
                           message: String,
                           file: StaticString,
                           line: Int,
                           function: StaticString ) {
      // Check for early exit when logger turned off
      guard logLevel != .off else { return }

      // Strip off the path information
      let rawFileName = String( describing: NSURL(fileURLWithPath: String( describing: file ) ).lastPathComponent! )
      // Get the appenders for this logger. As [String,AppenderProtocol]
      let thisLoggersLevel = level.rawValue
      let requestedLogLevel = logLevel.rawValue
      for ( _, appender ) in appenderList {
         // The WindowAppender makes its own decision regarding output
         if thisLoggersLevel <= requestedLogLevel || appender.appenderClass == Appenders.windowAppenderName {
            do {
               try appender.writeLog( name: name,
                                      level: logLevel,
                                      fileName: rawFileName,
                                      line: line,
                                      function: String( describing: function ),
                                      isFirstWrite: firstWrite,
                                      message: message )
            } catch let error as LoggerError {
               print( "Write log error: \(error.description)" )
            } catch {
               print( "Unknown error when writing log" )
            }
         }
      }
      // Finally turn off first write flag (which stays off until app. is rerun).
      firstWrite = false
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Set the logging level dynamically.
   ///
   /// - Parameters:
   ///   - level: The logging level as a string "info", "debug" etc

   public func set( level: String ) {
      self.level.value = level
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Get the current logging level.
   ///
   /// - Returns: the current logging level as LoggerLevel.

   public func getLevel() -> LoggerLevelEnum {
      return level.enumValue
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Check the current logging level.
   ///
   /// - Parameters:
   ///   - level: The logging level to check against
   /// - Returns: true if the current logging level is less than or equal to the input level

   public func check( level: LoggerLevelEnum ) -> Bool {
      let thisLoggersLevel = self.level.rawValue
      return thisLoggersLevel <= level.rawValue
   }

}

