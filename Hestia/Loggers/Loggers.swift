//
//  Loggers.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 04/03/2019.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

typealias LoggersListType = [ String: LoggerProtocol ]

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Loggers
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
/// The Loggers class maintains a list of Logger class
/// instances keyed by `name`.

class Loggers: LoggersProtocol {

   /// List of loggers keyed by name
   var loggersList = LoggersListType()

   init() {
      NotificationCenter.default.addObserver( self,
                                              selector: #selector( self.setLevel( _: ) ),
                                              name: LoggerFactory.setLoggerLevelMessage,
                                              object: nil )
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /// Does the logger exist?
   ///
   /// - Parameters:
   ///   - name: The name of the logger to find
   /// - Returns: true if exists

   func loggerExists( _ name: String ) -> Bool {
      return loggersList[ name ] != nil
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /// Find the logger in the list. If it is not in the list
   /// it throws an error.
   ///
   /// - Parameters:
   ///   - name: The name of the logger to find
   /// - Throws: LoggerError.cannotFindLogger

   func findLogger( _ name: String ) throws -> LoggerProtocol {
      guard loggerExists( name ) else {
         throw LoggerError.init(kind: ErrorKind.cannotFindLogger( name: name ) )
      }
      return loggersList[ name ]!
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   @objc func setLevel( _ notification: NSNotification ) {
      let mData = notification.object as! LoggerFactory.LoggerLevelMessageStruct
      // Reasonably safe to ignore the throw here.
      var logger = try? findLogger( mData.name ) as LoggerProtocol
      let level = mData.level as LoggerLevelEnum

      let d = level.description.lowercased().trimmingCharacters( in: .whitespaces )
      // print( "logger: \(logger?.name ?? ""), level.description: \(level.description), d: \(d)" )
      logger?.level.value = d
      // print( logger!.description )
   }

}
