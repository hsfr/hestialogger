//
//  LoggerErrors.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 17/09/2017.
//  Copyright (c) 2017 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Error types
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// The type of error (internal) for the logger

enum ErrorKind {
   case cannotCreateXMLParserForConfigurationFile( name: String )
   case cannotFindXMLConfigurationFile( name: String )
   case cannotReadXMLConfigurationFile( name: String, failure: String  )
   case cannotFindLogger( name: String )
   case cannotFindAppender( name: String )
   case cannotFindLayout( name: String )
   case cannotFindParameter( name: String )
   case cannotMoveLogFile( from: String, to: String, description: String )
   case cannotDeleteLogFile( name: String, description: String )
   case cannotWriteToLogFile( name: String, description: String )
   case unknownError
   
   var description: String {
      switch self {
         case .cannotCreateXMLParserForConfigurationFile( let name ): return "Cannot create XMLParse for configuration file '\(name)'"
         case .cannotFindXMLConfigurationFile( let name ): return "Cannot find configuration file '\(name)'"
         case .cannotReadXMLConfigurationFile( let name, let failure ): return "Cannot parse configuration file '\(name): \(failure)'"
         case .cannotFindLogger( let name ): return "Cannot find logger '\(name)' or root logger"
         case .cannotFindAppender( let name ): return "Cannot find appender '\(name)'"
         case .cannotFindLayout( let name ): return "Cannot find layout '\(name)'"
         case .cannotFindParameter( let name ): return "Cannot find parameter '\(name)'"
         case .cannotMoveLogFile( let from, let to, let description ): return "Cannot move '\(from)' to '\(to)'\n\(description)"
         case .cannotDeleteLogFile( let name, let description ): return "Cannot delete log file '\(name)'\n\(description)"
         case .cannotWriteToLogFile( let name, let description ): return "Cannot write to log file '\(name)'\n\(description)"
         case .unknownError: return "Unknown error"
      }
   }
   
   var suggestion: String {
      switch self {
         case .cannotCreateXMLParserForConfigurationFile( _ ): return "check file exists"
         case .cannotFindXMLConfigurationFile( _ ): return "check file exists"
         case .cannotReadXMLConfigurationFile( _, _ ): return "check file syntax"
         case .cannotFindLogger( _ ): return "check config file or calling source"
         case .cannotFindAppender( _ ): return "check config file"
         case .cannotFindLayout( _ ): return "check config file"
         case .cannotFindParameter( _ ): return "check config file"
         case .cannotMoveLogFile( _, _, _ ): return "check file/folder permissions"
         case .cannotDeleteLogFile( _, _ ): return "check file/folder permissions"
         case .cannotWriteToLogFile( _, _ ): return "check file/folder permissions"
         case .unknownError: return "No suggestion"
      }
   }
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Protocol for the internal error

protocol LoggerErrorProtocol: Error {
   
   /// The type of error
   var kind: ErrorKind { get set }
   
   /// Is this error fatal?
   var isFatal: Bool { get set }
   
   /// Output the description of the error
   ///
   /// - Returns: Test representaion of the error
   
   var description: String { get }
   
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// A single error instance to report internal logger errors.

public struct LoggerError: LoggerErrorProtocol {
   
   var kind: ErrorKind = .unknownError
   
   var isFatal: Bool = false
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   ///
   /// The text representaion of the error.
   
   public var description: String {
      let mess = "\n**** \(kind.description): \(kind.suggestion)"
      return mess
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   ///
   /// Instantiate an instance of an error.
   ///
   /// - Parameters:
   ///   - kind: The type of the error
   ///   - isFatal: `true` if the error is considered fatal.
   
   init( kind: ErrorKind, isFatal: Bool = false ) {
      self.kind = kind
      self.isFatal = isFatal
   }
   
}

