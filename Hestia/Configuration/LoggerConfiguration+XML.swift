//
//  LoggerConfiguration+XML.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 24/04/2019.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

let CONFIGURATION_TAG = "configuration"

let APPENDERS_TAG = "appenders"
let APPENDER_TAG = "appender"
let APPENDER_NAME_ATTRIB = "name"
let APPENDER_CLASS_ATTRIB = "class"

let LAYOUT_TAG = "layout"
let LAYOUT_CLASS_ATTRIB = "class"

let PARAM_TAG = "param"
let PARAM_NAME_ATTRIB = "name"
let PARAM_VALUE_ATTRIB = "value"

let LOGGERS_TAG = "loggers"
let ROOT_TAG = "root"
let LOGGER_TAG = "logger"
let LOGGER_NAME_ATTRIB = "name"

let LEVEL_TAG = "level"
let LEVEL_VALUE_ATTRIB = "value"

let APPENDER_REF_TAG = "appender-ref"
let APPENDER_REF_ATTRIB = "ref"

let ROOT_LOGGER_NAME = "root"

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - LoggerConfiguration Class
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Stores the configuration.
///
/// The data was read from the external configuration
/// file. The file is formatted in XML and `LoggerConfiguration`
/// provides the XML parser delegate.

extension LoggerConfiguration :  XMLParserDelegate {
   
   /// The state machine's states for reading the XML
   enum ParserState {
      case start
      case configuration
      case appenders
      case appender
      case loggers
      case logger
      case layout
      case level
      case appender_ref
      case parameter
   }
   
   // The following horrors are to overcome the problem that extensions
   // may not contain stored properties :-(
   
   struct Components {
      static var _newAppender: Appender!
      static var _newLogger: Logger!
      static var _newLayout: Layout!
      static var _newLevel: Level!
      static var _newParameter: Parameter!
      static var _newAppenderRef: String!
      static var _xmlState = ParserState.start
   }
   
   /// Where the Appender is assembled
   internal var newAppender: Appender {
      get {
         return Components._newAppender
      }
      set( newValue ) {
         Components._newAppender = newValue
      }
   }
   
   /// Where the Logger is assembled
   internal var newLogger: Logger {
      get {
         return Components._newLogger
      }
      set( newValue ) {
         Components._newLogger = newValue
      }
   }
   
   /// Where the Layout is assembled
   internal var newLayout: Layout {
      get {
         return Components._newLayout
      }
      set( newValue ) {
         Components._newLayout = newValue
      }
   }
   
   /// Where the Level is assembled
   internal var newLevel: Level {
      get {
         return Components._newLevel
      }
      set( newValue ) {
         Components._newLevel = newValue
      }
   }
   
   /// Where the Parameter is assembled
   internal var newParameter: Parameter {
      get {
         return Components._newParameter
      }
      set( newValue ) {
         Components._newParameter = newValue
      }
   }
   
   /// The reference to the Appender (only a simple String)
   internal var newAppenderRef: String {
      get {
         return Components._newAppenderRef
      }
      set( newValue ) {
         Components._newAppenderRef = newValue
      }
   }
   
   /// The state machine's current state
   internal var xmlState: ParserState {
      get {
         return Components._xmlState
      }
      set( newValue ) {
         Components._xmlState = newValue
      }
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - XML Delegate Methods
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Sent by a parser object to its delegate when it encounters a start tag for a given element.
   ///
   ///  - Parameters:
   ///    - parser: A parser object.
   ///    - didStartElement: A string that is the name of an element (in its start tag).
   ///    - namespaceURI: If namespace processing is turned on, contains the URI for the current namespace as a string object.
   ///    - qualifiedName: If namespace processing is turned on, contains the qualified name for the current namespace as a string object.
   ///    - attributes: A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values.
   
   func parser( _ parser: XMLParser,
                didStartElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String?,
                attributes attributeDict: [String : String] )
   {
      let attributesArray = attributeDict as NSDictionary
      
      switch ( elementName, xmlState ) {
            
         case ( CONFIGURATION_TAG, .start ):
            xmlState = .configuration
            
         case ( APPENDERS_TAG, .configuration ):
            xmlState = .appenders
            
         case ( APPENDER_TAG, .appenders ):
            xmlState = .appender
            // Create new appender entry
            let ident = attributesArray.value( forKey: APPENDER_NAME_ATTRIB ) as! String
            newAppender = Appender()
            newAppender.name = ident
            newAppender.appenderClass = attributesArray.value( forKey: APPENDER_CLASS_ATTRIB ) as! String

         case ( LAYOUT_TAG, .appender ):
            xmlState = .layout
            newLayout = Layout()
            newLayout.layoutClassName = attributesArray.value( forKey: LAYOUT_CLASS_ATTRIB ) as! String
            
         case ( LOGGERS_TAG, .configuration ):
            xmlState = .loggers
            
         case ( LOGGER_TAG, .loggers ):
            xmlState = .logger
            let ident = attributesArray.value( forKey: LOGGER_NAME_ATTRIB ) as! String
            newLogger = Logger( identifier: ident )
            newLogger.type = .userDefined
            newLogger.name = ident

         case ( ROOT_TAG, .loggers ):
            xmlState = .logger
            newLogger = Logger( identifier: "root" )
            newLogger.type = .root
            
         case ( PARAM_TAG, _ ):
            newParameter = Parameter()
            newParameter.name = attributesArray.value( forKey: PARAM_NAME_ATTRIB ) as! String
            newParameter.value = attributesArray.value( forKey: PARAM_VALUE_ATTRIB ) as! String
            
         case ( LEVEL_TAG, .logger ):
            newLevel = Level()
            newLevel.value = attributesArray.value( forKey: LEVEL_VALUE_ATTRIB ) as! String
            LoggerFactory.loggerInitialLevelList[ newLogger.name ] = newLevel.value
            
         case ( APPENDER_REF_TAG, .logger ):
            newAppenderRef = (attributesArray.value( forKey: APPENDER_REF_ATTRIB ) as? String)!
            // Add to the displayed menu list only if required for the logging window
            do {
               let appenderClass = try appenders.findAppender( newAppenderRef ).appenderClass
               if appenderClass == Appenders.windowAppenderName {
                  LoggerFactory.loggerNamesList.append( newLogger.name )
               }
            } catch {
               ()
            }
            
         default:
            // Flag error
            xmlState = .start
            
      }
      
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Sent by a parser object to its delegate when it encounters
   /// an end tag for a given element.
   ///
   /// - Parameters:
   ///   - parser: A parser object.
   ///   - didEndElement: A string that is the name of an element (in its end tag).
   ///   - namespaceURI: If namespace processing is turned on, contains the URI for the current namespace as a string object.
   ///  - attributes: A dictionary that contains any attributes associated with the element. Keys are the names of attributes, and values are attribute values.
   
   func parser( _ parser: XMLParser,
                didEndElement elementName: String,
                namespaceURI: String?,
                qualifiedName qName: String? )
   {
      switch ( elementName, xmlState ) {
            
         case ( CONFIGURATION_TAG, _ ):
            // Parsing is finished
            break
            
         case ( APPENDERS_TAG, _ ):
            xmlState = .configuration
            
         case ( APPENDER_TAG, _ ):
            xmlState = .appenders
            // Save the new appender away
            self.appenders.appendersList[ newAppender.name ] = newAppender
            
         case ( LAYOUT_TAG, _ ):
            xmlState = .appender
            newAppender.layout = newLayout
            
         case ( LOGGERS_TAG, _ ):
            xmlState = .configuration
            
         case ( LOGGER_TAG, _ ):
            xmlState = .loggers
            self.loggers.loggersList[ newLogger.name ] = newLogger
            
         case ( ROOT_TAG, _ ) :
            xmlState = .loggers
            self.loggers.loggersList[ ROOT_LOGGER_NAME ] = newLogger
            
         case ( LEVEL_TAG, _ ):
            xmlState = .logger
            newLogger.level = newLevel
            
         case ( APPENDER_REF_TAG, _ ) :
            // The appender ref is a simple string so add it to the list and set
            // a null entry which will be resolved when analysing.
            xmlState = .logger
            newLogger.appenderList[ newAppenderRef ] = Appender()
            
         case ( PARAM_TAG, .appender ):
            // param tags have no content so no end tag
            newAppender.parameters.list[ newParameter.name ] = newParameter
            
         case ( PARAM_TAG, .layout ):
            // param tags have no content so no end tag
            newLayout.parameters.list[ newParameter.name ] = newParameter
            
         default:
            // Flag error
            xmlState = .start
            
      }
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Sent by a parser object to provide its delegate with a string
   /// representing all or part of the characters of the current element.
   ///
   /// - Parameters:
   ///  - parser: A parser object.
   ///  - str: A string representing the complete or partial textual content of the current element..
   
   func parser( _ parser: XMLParser, foundCharacters str: String )
   {
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Sent by the parser object to the delegate when it begins parsing a document.
   ///
   /// - Parameters:
   ///   - parser: A parser object.
   
   func parserDidStartDocument( _ parser: XMLParser )
   {
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Sent by the parser object to the delegate when it has successfully completed parsing.
   ///
   /// - Parameters:
   ///   - parser: A parser object.
   
   func parserDidEndDocument( _ parser: XMLParser )
   {
      // Must reset the parser to cater for a new configuration file.
      xmlState = ParserState.start
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Sent by the parser object to the delegate when there is an error.
   ///
   /// - Parameters:
   ///   - parser: A parser object.
   ///   - parseError: An `Error` object describing the error.
   
   func parser( _ parser: XMLParser, parseErrorOccurred parseError: Error )
   {
      let lineNumber = parser.lineNumber
      let columnNumber = parser.columnNumber
      let localDescription = parseError.localizedDescription
      print( "Error reading configuration file: Line \(lineNumber), Column \(columnNumber): \(String(describing: localDescription))" )
   }
   
}
