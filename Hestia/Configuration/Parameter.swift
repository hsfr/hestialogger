//
//  Parameter.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 19/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
///
/// Defines the externally viewable methods for the
/// `Parameters` list.

protocol ParametersProtocol {

    /// A list of parameters keyed by name
    var list: [ String: ParameterProtocol ] { get set }
    
    /// Find the parameter in the list. If it is
    /// not in the list it throws an error.
    ///
    /// - Parameters:
    ///    - name: The class name of the layout to find
    /// - Returns: Entry from list as `ParameterProtocol` instance
    /// - Throws: LoggerError.cannotFindParameter
    
    func findParameter( _ name: String ) throws -> ParameterProtocol
    
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
///
/// Maintains a list of parameters for `Layout` and `Appender`
/// declarations.
///
/// For example the declaration
/// ````
/// <layout class="PatternLayout">
///    <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m"/>
/// </layout>
/// ````
/// would have a list
/// ````
/// {ConversionPattern}->{%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m}
/// ````
///
/// - Important: Must be a class not structure because of reference type requirement.

class Parameters: ParametersProtocol {
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    var list: [ String: ParameterProtocol ]
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //
    /// Initialise instance and clear the parameter list.

    init() {
        self.list = [:]
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    //
    /// Convenience function to check existence of parameter.
    ///
    /// - Parameters:
    ///   - name: Name of parameter to find
    /// - Returns `true` if found in list

    private func parameterExists( _ name: String ) -> Bool {
        return self.list[ name ] != nil
    }
    
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
    // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

    func findParameter( _ name: String ) throws -> ParameterProtocol {
        guard parameterExists( name ) else {
            throw LoggerError.init(kind: ErrorKind.cannotFindParameter( name: name ) )
        }
        return self.list[ name ]!
    }
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Protocol for a single parameter

protocol ParameterProtocol {
    /// Name of the parameter
    var name: String { get set }

    /// Value of the parameter
    var value: String { get set }
}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

class Parameter: ParameterProtocol {
    var name = ""
    var value = ""
}


