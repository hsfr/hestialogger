<?xml version="1.0" encoding="UTF-8"?>

<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!-- -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-* -->
<!--

	Schematron Schema for Hestia Logging Configuration

	Author : Hugh Field-Richards.
	Email  : hsfr@hsfr.org.uk

   Needs to be extended.

-->

<sch:schema
   xmlns:sch="http://purl.oclc.org/dsdl/schematron"
   queryBinding="xslt2"
   xmlns:sqf="http://www.schematron-quickfix.com/validator/process">

   <sch:ns prefix="hestia" uri="http://www.hsfr.org.uk/Schema/Hestia"/>

   <sch:pattern>
      <!-- Element 'hestia:appender' must have unique 'name' attribute -->
      <sch:rule context="//hestia:appender/@name">
         <sch:let name="duplicateAppender" value="."/>
         <sch:assert test="count(//hestia:appender/@name[. = current()]) = 1">Appender '<sch:value-of
               select="$duplicateAppender"/>' is not unique!</sch:assert>
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <!-- Logger element 'name' must refer to appender 'name'-->
      <sch:rule context="hestia:appender-ref/@ref">
         <sch:let name="appender" value="."/>
         <sch:assert test="//hestia:appender/@name[. = current()]">Appender '<sch:value-of select="$appender"/>' not
            found!</sch:assert>
      </sch:rule>
   </sch:pattern>

   <!-- Check for correct parameters in appenders -->
   <sch:pattern>
      <sch:rule context="hestia:appender[@class = 'NullAppender']">
         <sch:let name="appenderName" value="@name"/>
         <sch:let name="appenderClass" value="@class"/>
         <sch:assert test="count(*) = 0">'<sch:value-of select="$appenderClass"/>' '<sch:value-of select="$appenderName"
            />' should not have child elements!</sch:assert>
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="hestia:param[@name = 'append']">
         <sch:let name="value" value="@value"/>
         <sch:assert test="@value = 'true' or @value = 'false'">Append parameter must be either `true` or `false`, found
               '<sch:value-of select="@value"/>'</sch:assert>
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="hestia:appender[@class = 'RollingFileAppender' or @class = 'FileAppender']">
         <sch:let name="appenderName" value="@name"/>
         <sch:let name="appenderClass" value="@class"/>
         <sch:assert test="hestia:param/@name = 'append'">'<sch:value-of select="$appenderClass"/>' should have append
            parameter!</sch:assert>
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="hestia:appender[@class = 'RollingFileAppender']">
         <sch:let name="appenderName" value="@name"/>
         <sch:let name="appenderClass" value="@class"/>
         <sch:assert test="hestia:param/@name = 'maxFileSize'">'<sch:value-of select="$appenderClass"/>' should have
            maxFileSize parameter!</sch:assert>
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="hestia:appender[@class = 'RollingFileAppender']">
         <sch:let name="appenderName" value="@name"/>
         <sch:let name="appenderClass" value="@class"/>
         <sch:assert test="hestia:param/@name = 'maxBackupIndex'">'<sch:value-of select="$appenderClass"/>' should have
            maxBackupIndex parameter!</sch:assert>
      </sch:rule>
   </sch:pattern>

   <sch:pattern>
      <sch:rule context="hestia:appender[@class = 'RollingFileAppender' or @class = 'FileAppender']">
         <sch:let name="appenderName" value="@name"/>
         <sch:let name="appenderClass" value="@class"/>
         <sch:assert test="hestia:param/@name = 'file'">'<sch:value-of select="$appenderClass"/>' should have file
            parameter!</sch:assert>
      </sch:rule>
   </sch:pattern>

</sch:schema>
