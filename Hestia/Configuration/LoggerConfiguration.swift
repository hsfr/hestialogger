//
//  LoggerConfiguration.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 17/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - LoggerConfiguration Class
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Stores the configuration.
///
/// The data was read from the external configuration
/// file. The file is formatted in XML and `LoggerConfiguration`
/// provides the XML parser delegate.

class LoggerConfiguration : NSObject {
   
   /// The name of the config file
   private var configurationFile: String
   
   /// The path to the config file
   private var configurationFileFolder: String
   
   /// The pointer to the list of loggers
   var loggers: Loggers
   
   /// The pointer to the list of appenders
   var appenders: Appenders
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Initialise a configuration class.
   ///
   /// - Parameters:
   ///   - loggers: A ref to the loggers list
   ///   - appenders: A ref to the appenders list
   ///   - configFile: Optional name of configuration file (defaults to "hestia.xml")
   ///   - path: Optional path to configuration file  (defaults to "")
   
   init( loggers: inout Loggers, appenders: inout Appenders, configFile: String = "hestia.xml", path: String = "" )
   {
      // Check for path separator
      self.configurationFileFolder = path
      self.configurationFile = configFile
      self.loggers = loggers
      self.appenders = appenders
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Input XML-based configuration from a file.
   ///
   /// Analyse the XML and build a simple data base in the loggers and
   /// appenders properties.
   ///
   /// - Throws: LoggerError.cannotReadXMLConfigurationFile
   
   func importXMLConfiguration() throws
   {
      let fileManager = FileManager.default
      if  !self.configurationFileFolder.isEmpty &&  self.configurationFileFolder.last != "/" {
         self.configurationFileFolder.append( "/" )
      }
      
      let fullFileName = "\(self.configurationFileFolder)\(self.configurationFile)"
      guard fileManager.fileExists( atPath: fullFileName ) else {
         throw LoggerError.init( kind: ErrorKind.cannotFindXMLConfigurationFile( name: fullFileName ), isFatal: true )
      }
            
      if let xmlParser = XMLParser( contentsOf: URL( fileURLWithPath: fullFileName ) ) {
         xmlParser.delegate = self
         guard ( xmlParser.parse() ) else {
            let lineNumber = String(describing: xmlParser.lineNumber)
            let columnNumber = String(describing: xmlParser.columnNumber)
            let localDescription = String(describing: xmlParser.parserError?.localizedDescription)
            let failureDescription = "Error reading configuration file: Line \(lineNumber), Column \(columnNumber): \(localDescription)"
            throw LoggerError.init( kind: ErrorKind.cannotReadXMLConfigurationFile(name: fullFileName, failure: failureDescription ), isFatal: true )
         }
      } else {
         throw LoggerError.init( kind: ErrorKind.cannotCreateXMLParserForConfigurationFile( name: fullFileName ), isFatal: true )
      }
      
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Analyse the XML inputted config.
   ///
   /// Doing this saves time later and associates the various
   /// appenders and loggers together.
   ///
   /// - Throws: LoggerError
   
   func analyse() throws
   {
      let namespace = LoggerFactory.hestiaLoggerNameSpace
      do {
         // Go through the appenders and add the layouts (only one layout per appender)
         for ( _, var appender ) in appenders.appendersList {
            if !appender.layout.layoutClassName.isEmpty {
               let layoutClassName = appender.layout.layoutClassName
               let layoutClass = NSClassFromString( "\(namespace).\(layoutClassName)" ) as! Layout.Type
               let lc = layoutClass.init()
               lc.clone( appender.layout )
               appender.layout = lc
            }
         }
         
         // Go through the loggers and add the appenders (multiple appenders per logger)
         for ( _, var logger ) in loggers.loggersList {
            // For each appender in the loggers list of appenders [String, AppenderProtocol]
            for ( appenderName, _ ) in logger.appenderList {
               let appenderInstance = try appenders.findAppender( appenderName )
               let appenderClassName = appenderInstance.appenderClass
               let createdClass = NSClassFromString( "\(namespace).\(appenderClassName)" ) as! Appender.Type
               let cc = createdClass.init()
               // Transfer data
               cc.clone( appenderInstance )
               logger.appenderList[ appenderName ] = cc
            }
         }
         
      } catch let e as LoggerError {
         throw e
      }
   }
   
}

