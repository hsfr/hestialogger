//
//  LoggerFactory.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 11/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - LoggerFactory Class
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/**

 `LoggerFactory` is the primary class for the logging system.
 
 ## Description
 
 Always make sure that you instantiate this class
 to use it.

 ### Usage example

 ```swift
 var loggerFactory: LoggerFactory!
 var logger: LoggerProtocol!

 setLoggingSystem()

 func setLoggingSystem() {
    do {
       loggerFactory = LoggerFactory.sharedInstance
       try loggerFactory.configure()
       logger = try loggerFactory.getLogger( name: "Test01.console" )
       logger.info( "Logger initialised" )
    } catch let e as LoggerError {
       print( e.description() )
    } catch {
       print( "Unknown error when setting logging system"  )
    }
 }
 ```

 Assuming a config file
 ```xml
 <configuration xmlns="http://www.hsfr.org.uk/Schema/Hestia">

    <appenders>
       <appender name="console" class="StdOutAppender">
          <layout class="PatternLayout">
            <param name="ConversionPattern" value="%d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m"/>
          </layout>
       </appender>
    </appenders>

    <loggers>
       <root>
          <level value="fatal"/>
          <appender-ref ref="console"/>
       </root>
       <logger name="Test01.console">
          <level value="info"/>
          <appender-ref ref="console"/>
       </logger>
    </loggers>

 </configuration>
 ```

 Typical output would be (the line number and file are only typical)
 ```
 01/06/2018 05:29: Debug main.swift [54] infoTest(): Logger initialised
 ```
 */

public class LoggerFactory {
   
   static let defaultConfigFileName = "hestia-config"
  
   static let defaultConfigFileExtension = "xml"

   /// Instance variable when creating factory
   static public let sharedInstance = LoggerFactory()

   /// Use this variable to help terminal based applications.
   ///
   /// Typical use (assuming application's name is `TerminalAppUsingHestia`):
   ///
   /// ```swift
   ///     LoggerFactory.programNamespace = "TerminalAppUsingHestia"
   ///
   ///     var loggerFactory: LoggerFactory = LoggerFactory.sharedInstance
   ///
   ///     do {
   ///        try loggerFactory.configure()
   ///     } catch let e as LoggerError {
   ///        print( e.description() )
   ///     } catch {
   ///        print( "Unknown error when setting logging system"  )
   ///     }
   /// ```
   ///
   /// The default value is used when a Framework is in a Cocoa app.
   static public var programNamespace: String = "HestiaLogger"

   /// Class variable that returns the name space of the app.
   ///
   /// It is used when
   /// analysing and building the appenders and layouts.
   static public var hestiaLoggerNameSpace: String {
      // Done this way for future plans!
      return programNamespace
   }

   /// An array of the declared loggers
   var loggers = Loggers()

   /// An array of the declared appenders
   var appenders = Appenders()

   //static var currentLogger = ""

   /// Is there a window logging panel being displayed?
   static var isLoggerWindowLoaded = false

   /// The controller for window logger panel.
   static var appenderWindowController = NSWindowController()

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // Notification variables etc
   //
   /// The message identity

   static let displayLogMessageInWindow = NSNotification.Name( rawValue: "Hestia.displayLogMessageInWindow" )
   static let setLoggerLevelMessage = NSNotification.Name( rawValue: "Hestia.setLoggerLevelMessage" )

   /// The data structure for changing logger level.
   ///
   /// Only used to set initial value for WindowAppender
   struct LoggerLevelMessageStruct {
      var name: String
      var level: LoggerLevelEnum
   }

   /// The data structure for the posted window controller message.
   struct WindowAppenderMessageStruct {
      var name: String
      var layout: LayoutProtocol
      var level: LoggerLevelEnum
      var fileName: String
      var line: Int
      var function: String
      var  message: String
   }

   /// The data to be posted to the window controller.
   static var levelMessageData = LoggerLevelMessageStruct( name: "", level: LoggerLevelEnum.off )
   static var windowMessageData = WindowAppenderMessageStruct( name: "",
                                                               layout: Layout(),
                                                               level: LoggerLevelEnum.off,
                                                               fileName: "", line: 0, function: "", message: "" )

   /// Returns a version number together with build number.
   ///
   /// - Warning: Terminal only applications
   /// do not support this since they do not use frameworks.
   static public var version: String {
      let bundle = Bundle.init( for: self )
      if let dict = bundle.infoDictionary {
         if let version = dict["CFBundleShortVersionString"] as? String,
            let build = dict[ "CFBundleVersion" ] as? String {
            return "\(version).\(build)"
         }
      }
      return "Unknown version (terminal only apps do not support version number)"
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Configure the logger.
   ///
   /// Reads the properties file from the
   /// framework default XML configuration file (`hestia-config.xml`).
   ///
   /// - Throws: `LoggerError` if there is a problem reading/parsing the configuration file

   public func configure() throws
   {
      let bundle = Bundle.init( for: type( of: self ) )
      let fileURL = bundle.url( forResource: LoggerFactory.defaultConfigFileName, withExtension: LoggerFactory.defaultConfigFileExtension )
      let filePath = fileURL!.deletingLastPathComponent().path
      let config = LoggerConfiguration( loggers: &loggers,
                                        appenders: &appenders,
                                        configFile: "\(LoggerFactory.defaultConfigFileName).\(LoggerFactory.defaultConfigFileExtension)",
                                        path: filePath )
      try config.importXMLConfiguration()
      try config.analyse()
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Configure the logger.
   ///
   /// Reads the properties file from the
   /// supplied XML configuration file in the bundle resources directory.
   ///
   /// - Parameters:
   ///   - from: the XML configuration file with the configuration data
   /// - Throws: `LoggerError` if there is a problem reading/parsing the configuration file

   public func configure( from: String ) throws
   {
      let defaultConfigFileName = from.fileName
      let defaultConfigFileExtension = from.fileExtension
      let bundle = Bundle.init( for: type( of: self ) )
      let fileURL = bundle.url( forResource: defaultConfigFileName, withExtension: defaultConfigFileExtension )
      let filePath = fileURL!.deletingLastPathComponent().path
      let config = LoggerConfiguration( loggers: &loggers,
                                        appenders: &appenders,
                                        configFile: "\(defaultConfigFileName).\(defaultConfigFileExtension)",
         path: filePath )
      try config.importXMLConfiguration()
      try config.analyse()
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Configure the logger.
   ///
   /// Reads the properties file from the
   /// supplied XML configuration file.
   ///
   /// - Parameters:
   ///   - from: the XML configuration file with the configuration data
   ///   - inFolder: the folder where the configuration file is held
   /// - Throws: `LoggerError` if there is a problem reading/parsing the configuration file

   public func configure( from: String, inFolder: String ) throws
   {
      let fullFileName = "\(inFolder)/\(from)"
      guard LoggerFactory.fileExists( fullFileName ) else {
         throw LoggerError.init( kind: .cannotFindXMLConfigurationFile( name: fullFileName ), isFatal: true )
      }

      let config = LoggerConfiguration( loggers: &loggers,
                                        appenders: &appenders,
                                        configFile: from,
                                        path: inFolder )
      try config.importXMLConfiguration()
      try config.analyse()
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Get a logger from the list of loggers.
   ///
   /// - Parameters:
   ///   - name: The name of the logger to fetch
   /// - Returns: A logger instance for this named logger (`LoggerProtocol`)
   /// - Throws: `LoggerError` if cannot find the requested logger or the root logger.

   public func getLogger( name: String ) throws -> Logger
   {
      var thisLogger: LoggerProtocol
      if loggers.loggerExists( name ) {
         // Safely ignore exception since we know that logger exists here
         thisLogger = try! loggers.findLogger( name )
      } else {
         // Check to see whether the root logger exists (it should)
         do {
            thisLogger = try loggers.findLogger( ROOT_LOGGER_NAME )
         } catch let e as LoggerError {
            throw e
         }
      }
      return thisLogger as! Logger
   }

}
