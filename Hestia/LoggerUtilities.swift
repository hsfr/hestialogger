//
//  LoggerUtilities.swift
//  ExtractData
//
//  Created by Hugh Field-Richards on 27/10/2014.
//  Copyright (c) 2014 Hugh Field-Richards. All rights reserved.
//

import Foundation


// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Extensions for outputting text to file
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension String {

   /// Extract file name from string
  var fileName: String {
      if let fileNameWithoutExtension = NSURL(fileURLWithPath: self).deletingPathExtension?.lastPathComponent {
         return fileNameWithoutExtension
      } else {
         return ""
      }
   }

   /// Extract path name from file name as string
   var pathName: String {
      if let pathName = NSURL(fileURLWithPath: self).baseURL?.absoluteURL.path {
         return pathName
      } else {
         return ""
      }
   }

    /// Extract file extension from string
   var fileExtension: String {
      if let fileExtension = NSURL(fileURLWithPath: self).pathExtension {
         return fileExtension
      } else {
         return ""
      }
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /// Append string to the end of a file
   ///
   /// - Parameters:
   ///   - toFile: destination file name (+path)
   /// - Throws: System exception if problem

   func appendToFile( toFile: String ) throws
   {
      let data = self.data( using: String.Encoding.utf8 )!
      try data.append( toFile: toFile )
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension Data {

   /// Append data to the end of a file
   ///
   /// - Parameters:
   ///   - toFile: destination file name (+path)
   /// - throws: System exception if problem

   func append( toFile: String ) throws
   {
      if let fileHandle = FileHandle( forWritingAtPath: toFile ) {
         // Closes file whatever happens
         defer {
            fileHandle.closeFile()
         }
         fileHandle.seekToEndOfFile()
         fileHandle.write( self )
      }
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Extensions for debugging XML
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// The following extensions are only here for debugging
// the XML configuration input.

extension LoggerProtocol {

   var outputXML: String {
      var result = LoggerFactory.outputTagWithAttributes( LEVEL_TAG, attrib: ( LEVEL_VALUE_ATTRIB, level.value ) )
      for ( appenderName, _ ) in appenderList {
         result += LoggerFactory.outputTagWithAttributes( APPENDER_REF_TAG, attrib: ( APPENDER_REF_ATTRIB, appenderName ) )
      }
      switch type {
      case .userDefined:
         return LoggerFactory.outputTagWithAttributes( LOGGER_TAG, attrib: ( LOGGER_NAME_ATTRIB, name ), contents: result)
      case .root:
         return LoggerFactory.outputTag( ROOT_TAG, contents: result )
      }
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension LoggerType {

   var outputXML: String {
      switch self {
      case .userDefined:
         return LOGGER_TAG
      case .root:
         return ROOT_TAG
      }
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension LevelProtocol {

   var outputXML: String {
      let result = LoggerFactory.outputTagWithAttributes( LEVEL_TAG, attrib: ( LEVEL_VALUE_ATTRIB, value ) )
      return result
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension AppendersProtocol {

   var outputXML: String {
      var result = ""
      for ( _, appender ) in appendersList {
         result += appender.outputXML
      }
      return LoggerFactory.outputTag( APPENDERS_TAG, contents: result )
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension AppenderProtocol {

   var outputXML: String {
      var result = ""
      for ( _, parameter ) in parameters.list {
         result += parameter.outputXML
      }
      result += layout.outputXML
      return LoggerFactory.outputTagWithAttributes( APPENDER_TAG, attrib: ( APPENDER_NAME_ATTRIB, name ), ( APPENDER_CLASS_ATTRIB, appenderClass ), contents: result )
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension LayoutProtocol {

   var outputXML: String {
      var result = ""
      for ( _, parameter ) in parameters.list {
         result += parameter.outputXML
      }
      return LoggerFactory.outputTagWithAttributes( LAYOUT_TAG, attrib: ( LAYOUT_CLASS_ATTRIB, layoutClassName ), contents: result )
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension ParameterProtocol {

   var outputXML: String {
      let result = LoggerFactory.outputTagWithAttributes( PARAM_TAG, attrib: ( PARAM_NAME_ATTRIB, name ), ( PARAM_VALUE_ATTRIB, value ) )
      return result
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

extension LoggersProtocol {

   var outputXML: String {
      var result = ""
      for ( _, value ) in loggersList {
         result += value.outputXML
      }
      return LoggerFactory.outputTag( LOGGERS_TAG, contents: result )
   }

}

