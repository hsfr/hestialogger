//
//  WindowAppenderViewController.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 25/05/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation
import Cocoa
import AppKit
import CoreGraphics

class WindowAppenderViewController: NSViewController, NSTextViewDelegate {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   /// A list of loggers
   /// ```xml
   /// <logger name="Test01.xml">
   ///    ...
   /// </logger>
   /// ```
   typealias LoggerMenuArrayType = Array<LoggerDataStruct>

   /// Logger details structure
   ///
   /// The data here is used to decide whether a logger should be displayed
   /// and what colour it should make the text. We cannot use a Dictionary here
   /// because the menus have not had a logger allocated yet. The only things
   /// that will be set initially are the colour and the enabled (false).
   struct LoggerDataStruct {
      var name: String
      var level: LoggerLevelEnum
      var colour: NSColor
      var enabled: Bool
   }

   /// A list of `LoggerDataStruct`
   var loggerMenuList = LoggerMenuArrayType()

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - Logger Variables/Outlets
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   // The outputs to popup buttons that allows the user to select the loggers.
   @IBOutlet weak var selectMenu_1: NSPopUpButton!
   @IBOutlet weak var selectMenu_2: NSPopUpButton!
   @IBOutlet weak var selectMenu_3: NSPopUpButton!
   @IBOutlet weak var selectMenu_4: NSPopUpButton!
   @IBOutlet weak var selectMenu_5: NSPopUpButton!
   @IBOutlet weak var selectMenu_6: NSPopUpButton!
   @IBOutlet weak var selectMenu_7: NSPopUpButton!
   @IBOutlet weak var selectMenu_8: NSPopUpButton!
   @IBOutlet weak var selectMenu_9: NSPopUpButton!
   @IBOutlet weak var selectMenu_10: NSPopUpButton!
   @IBOutlet weak var selectMenu_11: NSPopUpButton!
   @IBOutlet weak var selectMenu_12: NSPopUpButton!

   typealias SelectMenuArrayType = Array<NSPopUpButton>

   /// A list of the select popup buttons.
   var selectMenuArray: SelectMenuArrayType = []

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /// The associated colors for the loggers.
   var menuLogColours: [NSColor] = {
      if #available(OSX 10.15, *) {
         return [
            NSColor.systemRed,
            NSColor.systemGreen,
            NSColor.systemBlue,
            NSColor.systemOrange,
            NSColor.systemYellow,
            NSColor.systemBrown,
            NSColor.systemPink,
            NSColor.systemPurple,
            NSColor.systemTeal,
            NSColor.systemIndigo,
            NSColor.systemGray,
            NSColor( red:0.094, green:0.655, blue:0.710, alpha:1.0 ) // teal blue
         ]
      } else {
         return [
            NSColor.systemRed,
            NSColor.systemGreen,
            NSColor.systemBlue,
            NSColor.systemOrange,
            NSColor.systemYellow,
            NSColor.systemBrown,
            NSColor.systemPink,
            NSColor.systemPurple,
            NSColor.systemTeal,
            NSColor.systemRed,
            NSColor.systemGray,
            NSColor( red:0.094, green:0.655, blue:0.710, alpha:1.0 )
         ]
      }
   }()

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - Level Variables/Outlets
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   @IBOutlet var levelMenu_1: NSPopUpButton!
   @IBOutlet var levelMenu_2: NSPopUpButton!
   @IBOutlet var levelMenu_3: NSPopUpButton!
   @IBOutlet var levelMenu_4: NSPopUpButton!
   @IBOutlet var levelMenu_5: NSPopUpButton!
   @IBOutlet var levelMenu_6: NSPopUpButton!
   @IBOutlet var levelMenu_7: NSPopUpButton!
   @IBOutlet var levelMenu_8: NSPopUpButton!
   @IBOutlet var levelMenu_9: NSPopUpButton!
   @IBOutlet var levelMenu_10: NSPopUpButton!
   @IBOutlet var levelMenu_11: NSPopUpButton!
   @IBOutlet var levelMenu_12: NSPopUpButton!

   typealias LevelMenuArrayType = Array<NSPopUpButton>

   /// A list of the level popup buttons.
   var levelMenuArray: LevelMenuArrayType = []

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /// The view area that the logs are written to.
   @IBOutlet var logTextPanel: NSTextView!

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - Override Instance Methods
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   // View loaded so set up menus.

   override func viewDidLoad() {
      super.viewDidLoad()

      // Set up the logger select menus with possible loggers from configuration file.
      selectMenuArray = [ selectMenu_1, selectMenu_2, selectMenu_3, selectMenu_4,
                          selectMenu_5, selectMenu_6, selectMenu_7, selectMenu_8,
                          selectMenu_9, selectMenu_10, selectMenu_11, selectMenu_12 ]
      var idx = 0
      for menu in selectMenuArray {
         menu.removeAllItems()
         menu.addItem( withTitle: "Select logger" )
         for name in LoggerFactory.loggerNamesList {
            menu.addItem( withTitle: name )
         }
         if idx < LoggerFactory.loggerNamesList.count {
            menu.selectItem( withTitle: LoggerFactory.loggerNamesList[idx] )
         }
         idx += 1
     }

      if !LoggerFactory.isLoggerWindowLoaded {
         for idx in 0..<menuLogColours.count {
            let colour = menuLogColours[ idx ]
            // We don't know the name of the level yet so set them to be arbitrary values.
            loggerMenuList.append( LoggerDataStruct( name: "", level: LoggerLevelEnum.trace, colour: colour, enabled: false ) )
         }
      }

      levelMenuArray = [ levelMenu_1, levelMenu_2, levelMenu_3, levelMenu_4,
                         levelMenu_5, levelMenu_6, levelMenu_7, levelMenu_8,
                         levelMenu_9, levelMenu_10, levelMenu_11, levelMenu_12 ]
      idx = 0
      for menu in levelMenuArray {
         menu.removeAllItems()
         // Set up the pull down menu with the levels
         for i in 0..<LoggerLevelEnum.count {
            // We know all the levels since they are preset in Hestia
            menu.addItem( withTitle: LoggerLevelEnum(rawValue: i)?.description ?? "" )
            menu.setTitle( LoggerLevelEnum.off.description )
            menu.isEnabled = false
         }

         // Now to set the lebel for the logger
         if idx < LoggerFactory.loggerNamesList.count {
            let loggerName = LoggerFactory.loggerNamesList[idx]
            let initialLevel = getInitialLogLevelFor( loggerName )
            let level = initialLevel.description
       
            levelMenuArray[idx].selectItem( withTitle: level )
            levelMenuArray[idx].isEnabled = true

            loggerMenuList[idx].level = initialLevel
            loggerMenuList[idx].enabled = true
            loggerMenuList[idx].name = loggerName
         }
         idx += 1
      }
      
      // We use this for the `WindowAppender` to send log messages to this window controller.
      NotificationCenter.default.addObserver( self,
                                              selector: #selector( self.outputLog( _: ) ),
                                              name: LoggerFactory.displayLogMessageInWindow,
                                              object: nil )
      LoggerFactory.isLoggerWindowLoaded = true
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   override func viewDidDisappear() {
      NotificationCenter.default.removeObserver( self, name: LoggerFactory.displayLogMessageInWindow, object: nil )
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - Instance Methods
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Output log message to window.
   ///
   /// This is where all the work is done. If a logger is not
   /// on the list of selected loggers then it quits. Otherwise
   /// it sets up text of the right colour and font and appends it
   /// to the logging view.
   ///
   /// It is stimulated via a posted message from `WindowAppender`

   @objc func outputLog( _ notification: NSNotification )
   {
      DispatchQueue.main.async { [self] in
         let mData = notification.object as! LoggerFactory.WindowAppenderMessageStruct
         let loggerName = mData.name
         let loggerLevel = mData.level
         let loggerFileName = mData.fileName
         let line = mData.line
         let functionName = mData.function
         let message = mData.message
         var msg = mData.layout.formatLog( level: loggerLevel,
                                           fileName: loggerFileName,
                                           line: line,
                                           function: functionName,
                                           message: message )
         msg += "\n"

         let levelForThisLogger = getLevelFor( loggerName )
         if levelForThisLogger.rawValue <= mData.level.rawValue {
            if loggerListContains( loggerName ) {
               if #available(OSX 10.15, *) {
                  let font = NSFont.monospacedSystemFont( ofSize: 10, weight: NSFont.Weight.regular )
                  let attributes: [NSAttributedString.Key: Any] = [
                     .font: font,
                     .foregroundColor: getColourFor( loggerName ),
                  ]
                  logTextPanel.textStorage?.append( NSAttributedString( string: msg, attributes: attributes  ) )
                  logTextPanel.scrollToEndOfDocument( nil )
               } else {
                  logTextPanel.string += ( "\(msg)" )
               }
            }
         }
      }
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   @IBAction func clearLoggingDisplay(_ sender: Any) {
      logTextPanel.string = ""
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func saveLoggingText(_ sender: Any) {
      do {
         let savePanel = NSSavePanel()
         savePanel.isFloatingPanel = true
         savePanel.allowedFileTypes = [ "log" ]
         savePanel.directoryURL = URL( fileURLWithPath: NSHomeDirectory() )
         let result = savePanel.runModal()
         if result == NSApplication.ModalResponse.OK {
            let url = savePanel.url!
            try logTextPanel.string.write( to: url, atomically: false, encoding: String.Encoding.utf8 )
         }
      } catch {
         () // Ignored at present
      }
   }
   
}
