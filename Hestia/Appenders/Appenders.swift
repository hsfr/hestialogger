//
//  Appenders.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 08/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

typealias AppendersListType = [ String: AppenderProtocol ]

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// Must be a class not structure because of reference type
// requirement.

class Appenders: AppendersProtocol {
   
   static let windowAppenderName = "WindowAppender"
   
   var appendersList = AppendersListType()
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Does the appender exist?
   ///
   /// - Parameters:
   ///   - name: The name of the appender to find
   /// - Returns: `true` if appender exists
   
   private func appenderExists( _ name: String ) -> Bool {
      return appendersList[ name ] != nil
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Find the appender in the list. If it is not in the list
   /// it throws an error.
   ///
   /// - Parameters:
   ///   - name: The name of the appender to find
   /// - Throws: LoggerError.cannotFindAppender

   func findAppender( _ name: String ) throws -> AppenderProtocol {
      guard appenderExists( name ) else {
         throw LoggerError.init( kind: ErrorKind.cannotFindAppender( name: name ) )
      }
      return appendersList[ name ]!
   }

}

