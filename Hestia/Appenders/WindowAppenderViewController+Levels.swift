//
//  WindowAppenderViewController+Levels.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 04/06/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation
import Cocoa

extension WindowAppenderViewController {

   internal func setLevelForMenu( _ menu: Int, itemIndex: Int, title: String ) {
      let menuIndex = menu - 1
      let correctedTitle = title.lowercased().trimmingCharacters( in: .whitespaces )
      guard itemIndex >= LoggerLevelEnum.trace.rawValue && itemIndex <= LoggerLevelEnum.off.rawValue else {
         return
      }
      loggerMenuList[menuIndex].level = Level( value: correctedTitle ).enumValue
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   internal func getInitialLogLevelFor( _ name: String ) -> LoggerLevelEnum {
      for ( key, level ) in LoggerFactory.loggerInitialLevelList {
         if name == key {
            return Level( value: level ).enumValue
         }
      }
      return LoggerLevelEnum.off
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - Level Menu Action Methods
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   // There has to be a better way of doing this.

   @IBAction func levelMenuAction_1( _ sender: Any ) {
      setLevelForMenu( 1, itemIndex: levelMenu_1.indexOfSelectedItem, title: levelMenu_1.title  )
   }

   @IBAction func levelMenuAction_2( _ sender: Any ) {
      setLevelForMenu( 2, itemIndex: levelMenu_2.indexOfSelectedItem, title: levelMenu_2.title  )
   }

   @IBAction func levelMenuAction_3( _ sender: Any ) {
      setLevelForMenu( 3, itemIndex: levelMenu_3.indexOfSelectedItem, title: levelMenu_3.title  )
   }

   @IBAction func levelMenuAction_4( _ sender: Any ) {
      setLevelForMenu( 4, itemIndex: levelMenu_4.indexOfSelectedItem, title: levelMenu_4.title  )
   }

   @IBAction func levelMenuAction_5( _ sender: Any ) {
      setLevelForMenu( 5, itemIndex: levelMenu_5.indexOfSelectedItem, title: levelMenu_5.title  )
   }

   @IBAction func levelMenuAction_6( _ sender: Any ) {
      setLevelForMenu( 6, itemIndex: levelMenu_6.indexOfSelectedItem, title: levelMenu_6.title  )
   }

   @IBAction func levelMenuAction_7( _ sender: Any ) {
      setLevelForMenu( 7, itemIndex: levelMenu_7.indexOfSelectedItem, title: levelMenu_7.title  )
   }

   @IBAction func levelMenuAction_8( _ sender: Any ) {
      setLevelForMenu( 8, itemIndex: levelMenu_8.indexOfSelectedItem, title: levelMenu_8.title  )
   }

   @IBAction func levelMenuAction_9( _ sender: Any ) {
      setLevelForMenu( 9, itemIndex: levelMenu_9.indexOfSelectedItem, title: levelMenu_9.title  )
   }

   @IBAction func levelMenuAction_10( _ sender: Any ) {
      setLevelForMenu( 10, itemIndex: levelMenu_10.indexOfSelectedItem, title: levelMenu_10.title  )
   }

   @IBAction func levelMenuAction_11( _ sender: Any ) {
      setLevelForMenu( 11, itemIndex: levelMenu_11.indexOfSelectedItem, title: levelMenu_11.title  )
   }

   @IBAction func levelMenuAction_12( _ sender: Any ) {
      setLevelForMenu( 12, itemIndex: levelMenu_12.indexOfSelectedItem, title: levelMenu_12.title  )
   }

   

}
