//
//  LogWindow.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 24/05/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

class WindowAppender: Appender {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   // Not proud of some of the code I have written in this class
   // and its associated classes/structures.
   //
   /// Initialise the `WindowAppender` when configuration file is loaded.
   ///
   /// If a window hasn't been created then do it. None of the select logger
   /// menus are set up here. That is done when a user selects which loggers
   /// they want to use.

   required init()
   {
      if !LoggerFactory.isLoggerWindowLoaded {
         let thisBundle = Bundle( for: WindowAppender.self )
         let storyboard = NSStoryboard( name: NSStoryboard.Name( Appenders.windowAppenderName ),
                                        bundle: thisBundle )
         let ident = NSStoryboard.SceneIdentifier( "Window Appender Controller" )
         LoggerFactory.appenderWindowController = storyboard.instantiateController( withIdentifier: ident ) as! NSWindowController
         LoggerFactory.appenderWindowController.showWindow( nil )
         LoggerFactory.appenderWindowController.window?.title = "Hestia Logger \(LoggerFactory.version)"
      }
      super.init()
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Write output to a logging window.
   ///
   /// - Parameters:
   ///   - name: The name of the logger making the call
   ///   - level: the log level for this logging statement
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - isFirstWrite: is this the first time the log file has been written to?
   ///   - message: the log message to output
   /// - Throws: `LoggerError.cannotFindParameter`, `ErrorKind.cannotWriteToLogFile`

   override func writeLog( name: String, level: LoggerLevelEnum, fileName: String, line: Int, function: String, isFirstWrite: Bool, message: String ) throws {
      // Rather nasty way of getting the data into the notification.
      LoggerFactory.windowMessageData.name = name
      LoggerFactory.windowMessageData.layout = layout
      LoggerFactory.windowMessageData.level = level
      LoggerFactory.windowMessageData.fileName = fileName
      LoggerFactory.windowMessageData.line = line
      LoggerFactory.windowMessageData.function = function
      LoggerFactory.windowMessageData.message = message
      NotificationCenter.default.post( name: LoggerFactory.displayLogMessageInWindow, object: LoggerFactory.windowMessageData )
   }

}
