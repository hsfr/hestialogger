//
//  FileAppender.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 19/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

class FileAppender: Appender {

   /// Is appender a file appender?
   override var isFileAppender: Bool {
      return true
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Write output to a single file.
   ///
   /// Each session overwrites the file (no session append) and the
   /// file can grow to any size.
   ///
   /// - Important: If the `xmlLayout` format is used an XML header is written out
   /// first to satisfy the requirements for well-formedness. This also
   /// requires that the logger is closed to ensure an XML footer
   /// (closing tag) is output; this is the responsibilty of the
   /// application designer.
   ///
   /// - Parameters:
   ///   - name: The name of the logger making the call
   ///   - level: the log level for this logging statement
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - isFirstWrite: is this the first time the log file has been written to?
   ///   - message: the log message to output
   /// - Throws: `LoggerError.cannotFindParameter`, `ErrorKind.cannotWriteToLogFile`, `ErrorKind.cannotDeleteLogFile`
   
   override func writeLog( name: String, level: LoggerLevelEnum, fileName: String, line: Int, function: String, isFirstWrite: Bool, message: String ) throws {
      
      let formattedOutput = layout.formatLog( level: level, fileName: fileName, line: line, function: function, message: "\(message)\n" )
      
      // Sort out the file to write to. Set a default value in case not found.
      var logFile: String
      var append: Bool
      
      do {
         logFile = try parameters.findParameter( Appender.fileAttrib ).value
      } catch {
         logFile = Appender.defaultLogFile
      }

      do {
         append = ( try parameters.findParameter( Appender.appendAttrib ).value ) == "true"
      } catch {
         append = false
      }
      
      let fileManager = FileManager.default
      // Only delete file if not appending or if it is first write
      if !append {
         // Not appending so we must start with a clean file if this
         // is the first write.
         if isFirstWrite {
            do {
               if LoggerFactory.fileExists( logFile ) {
                  try fileManager.removeItem( atPath: logFile )
               }
            } catch let error as NSError {
               throw LoggerError.init( kind: ErrorKind.cannotDeleteLogFile(name: logFile, description: error.localizedDescription ) )
            }
            
            do {
               if layout.isXmlLayout() {
                  try XmlLayout.openingTag.appendToFile( toFile: logFile )
               }
            } catch let error as NSError {
               throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile(name: logFile, description: error.localizedDescription ) )
            }
         }
      }
      
      if LoggerFactory.fileExists( logFile ) {
         do {
            try formattedOutput.appendToFile( toFile: logFile )
         } catch let error as NSError {
            throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile( name: logFile, description: error.localizedDescription ) )
         }
      } else {
         // Create the file and write to it.
         // If XML output put the header out first.
         do {
            fileManager.createFile( atPath: logFile, contents: Data(), attributes: nil )
            if layout.isXmlLayout() {
               try XmlLayout.openingTag.appendToFile( toFile: logFile )
            }
            try formattedOutput.appendToFile( toFile: logFile )
         } catch let error as NSError {
            throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile(name: logFile, description: error.localizedDescription ) )
         }
      }
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Close file assocated with appender.
   
   override func close() throws {
      // Sort out the file to close.
      //  Set a default value in case not found.
      var logFile = Appender.defaultLogFile
      logFile = try parameters.findParameter( Appender.fileAttrib ).value
      
      do {
         if layout.isXmlLayout() {
            try XmlLayout.closingTag.appendToFile( toFile: logFile )
         }
         
         let fileHandle: FileHandle? = FileHandle( forReadingAtPath: logFile )
         fileHandle?.closeFile()
      } catch let error as NSError {
         throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile(name: logFile, description: error.localizedDescription ) )
      }
   }
}



