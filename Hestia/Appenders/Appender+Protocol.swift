//
//  Appender+Protocol.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 25/05/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

typealias AppenderListType = [ String: AppenderProtocol ]

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Protocol for appender's base class.
///
/// - Note: The `writeLog` function is always overridden.

protocol AppenderProtocol {
   
   /// The name of the appender referenced by the logger
   var name: String { get set }

   /// The class name of the specific logger: `StdOutAppender` etc.
   var appenderClass: String { get set }

   /// A list of parameters. There can be 1 or more of these.
   var parameters: ParametersProtocol { get set }

   /// Each appender has a single layout associated with it.
   var layout: LayoutProtocol { get set }

   /// Is appender a file appender.
   var isFileAppender: Bool { get }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Write log out - always overridden
   ///
   /// - Parameters:
   ///   - name: The name of the logger making the call
   ///   - level: the log level for this logging statement
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - isFirstWrite: is this the first time the log file has been written to?
   ///   - message: the log message to output
   /// - Throws: error if problem

   func writeLog( name: String,  level: LoggerLevelEnum, fileName: String, line: Int, function: String, isFirstWrite: Bool, message: String ) throws

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Close file assocated with appender.
   ///
   /// Normally overriden only the appenders that use external files.

   func close() throws

}

