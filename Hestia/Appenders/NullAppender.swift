//
//  NullAppender.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 19/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

class NullAppender: Appender {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Does nothing.
   ///
   /// - Parameters:
   ///   - name: The name of the logger making the call
   ///   - level: the log level for this logging statement
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - firstWrite: is this the first time the log file has been written to?
   ///   - message: the log message to output
   /// - Throws: error if problem

   override func writeLog( name: String, level: LoggerLevelEnum, fileName: String, line: Int, function: String, isFirstWrite: Bool, message: String ) throws {
   }

}
