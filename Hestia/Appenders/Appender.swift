//
//  Appender.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 08/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
///
/// Appender's base class.

class Appender: AppenderProtocol {
   
   static var appendAttrib = "append"
   static var fileAttrib = "file"
   static var maxFileSizeAttrib = "maxFileSize"
   static var maxBackupIndexAttrib = "maxBackupIndex"
   static var nameSuffixAttrib = "nameSuffix"

   static let defaultLogFile = "hestia.log"
   
   /// The name of the appender referenced by the logger.
   var name: String = ""
   
   /// The class name of the specific appender: `StdOutAppender` etc.
   var appenderClass: String = ""
   
   /// The list of associated parameters associated with this appender.
   var parameters: ParametersProtocol
   
   /// The layout associated with this appender.
   var layout: LayoutProtocol
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Is appender a file appender?
   ///
   /// - Returns: `false` unless overridden
   
   var isFileAppender: Bool {
      return false
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   required init() {
      parameters = Parameters()
      layout = Layout()
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Write log out --- always overridden
   ///
   /// - Parameters:
   ///   - name: The name of the logger making the call
   ///   - level: the log level for this logging statement
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - firstWrite: is this the first time the log file has been written to?
   ///   - message: the log message to output
   /// - Throws: error if problem
   
   func writeLog( name: String, level: LoggerLevelEnum, fileName: String, line: Int, function: String, isFirstWrite: Bool, message: String ) throws {
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Clone a subclass from base class.
   ///
   /// A light-weight way to do things rather than use `NSCopy`.
   ///
   /// - Parameters:
   ///   - cl: the class to read into the new class
   
   func clone( _ cl: AppenderProtocol ) {
      name = cl.name
      appenderClass = cl.appenderClass
      parameters = cl.parameters
      layout = cl.layout
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Close file assocated with appender.
   ///
   /// Override this where necessary
   
   func close() throws {
   }
   
}

