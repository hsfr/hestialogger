//
//  Appenders+Protocol.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 25/05/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
// The AppendersProtocol defines a list of Appenders.

protocol AppendersProtocol {

   /// List of appenders keyed by name
   var appendersList: AppenderListType { get set }

   /// Find the appender in the list. If it is not in the list
   /// it throws an error.
   ///
   /// - Parameters:
   ///   - name: The name of the appender to find
   /// - Throws: LoggerError.cannotFindAppender

   func findAppender( _ name: String ) throws -> AppenderProtocol

}

