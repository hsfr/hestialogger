//
//  RollingFileAppender.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 19/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

class RollingFileAppender: Appender {
   
   /// Is appender a file appender?
   override var isFileAppender: Bool {
      return true
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Write output to a file.
   ///
   /// When file reaches a fixed maximum size it is pushed onto a
   /// rolling stack of files "`*.1`", "`*.2`" (or "'*-1.log'", "'*-2.log'"
   /// dependent on the _nameSuffix_ attribute being true) etc. The number of
   /// overspill files is set in the configuration file.
   ///
   /// - Important: If the `xmlLayout` format is used an XML header
   /// is written out first to satisfy the requirements for
   /// well-formedness. Bear in mind that the output may be spread
   /// across several files.
   ///
   /// - Parameters:
   ///   - name: The name of the logger making the call
   ///   - level: the log level for this logging statement
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - isFirstWrite: is this the first time the log file has been written to
   ///   - message: the log message to output
   /// - Throws: `LoggerError.cannotFindParameter`, `ErrorKind.cannotWriteToLogFile`, `ErrorKind.cannotDeleteLogFile`, ErrorKind.cannotMoveLogFile

   override func writeLog( name: String, level: LoggerLevelEnum, fileName: String, line: Int, function: String, isFirstWrite: Bool, message: String ) throws {
      
      let formattedOutput = layout.formatLog( level: level, fileName: fileName, line: line, function: function, message: "\(message)\n" )
      
      // Sort out the file to write to. Set some default values.
      var currentLogFile: String
      var maxFileSize: Int // in bytes
      var maxBackupIndex: Int
      var appending: Bool
      var nameSuffix: Bool
      
      var firstWrite = isFirstWrite
      
      do {
         currentLogFile = try parameters.findParameter( Appender.fileAttrib ).value
      } catch {
         currentLogFile = Appender.defaultLogFile
      }
      
      do {
         maxFileSize = Int( try parameters.findParameter( Appender.maxFileSizeAttrib ).value )!
      } catch {
         maxFileSize = 1000
      }
      
      do {
         maxBackupIndex = Int( try parameters.findParameter( Appender.maxBackupIndexAttrib ).value )!
      } catch {
         maxBackupIndex = 5
      }
      
      do {
         appending = ( try parameters.findParameter( Appender.appendAttrib ).value ) == "true"
      } catch {
         appending = false
      }
      
      // Convenience property
      let notAppending = !appending

      do {
         nameSuffix = ( try parameters.findParameter( Appender.nameSuffixAttrib ).value ) == "true"
      } catch {
         nameSuffix = false
      }
      
      let logFileName = NSString( string: currentLogFile ).lastPathComponent
      let logFilePath = NSString( string: currentLogFile ).deletingLastPathComponent
      let logFileNameNoExtension = NSString( string: logFileName ).deletingPathExtension
      let logFileNameExtension = NSString( string: logFileName ).pathExtension
      
      let fileManager = FileManager.default
      var fullFileName: String
      
      do {
         // Only delete files if not appending and it is first write.
         // We must remove all rolling files associated with the main log file.
         if notAppending && firstWrite {
            // First get a list of files to delete.
            let fileList = try FileManager.default.contentsOfDirectory( atPath: logFilePath )
            let logFileList = fileList.filter {
               $0.hasPrefix( logFileNameNoExtension )
            }
            for fileToDelete in logFileList {
               fullFileName = "\(logFilePath)/\(fileToDelete)"
               // print( "Removing: \(fullFileName)" )
               try fileManager.removeItem( atPath: fullFileName )
            }
//            let newFileList = try FileManager.default.contentsOfDirectory( atPath: logFilePath )
//            let newLogFileList = newFileList.filter {
//               $0.hasPrefix( logFileNameNoExtension )
//            }
//            for fileToDelete in newLogFileList {
//               fullFileName = "\(logFilePath)/\(fileToDelete)"
//               try fileManager.removeItem( atPath: fullFileName )
//            }
         }
      } catch let error as NSError {
         throw LoggerError.init( kind: ErrorKind.cannotDeleteLogFile(name: currentLogFile, description: error.localizedDescription ) )
      }
      
//      if notAppending {
//         // Clear all previous logs of this name if not appending.
//         // Get the list of files only
//         do {
//            let fileList = try FileManager().contentsOfDirectory( atPath: logFilePath )
//
//            let regex = try NSRegularExpression( pattern: "\(logFileNameNoExtension).*\\..*", options: NSRegularExpression.Options() )
//            for fileName in fileList {
//               let theRange = NSMakeRange( 0,  fileName.fileName.count )
//               if regex.numberOfMatches(in: fileName.fileName, options: [], range: theRange ) > 0 {
//                  print( fileName )
//               }
//            }
//         } catch let error as NSError {
//            throw LoggerError.init( kind: ErrorKind.cannotDeleteLogFile(name: fileName, description: error.localizedDescription ) )
//         }
//      }

      // If log file does not exist then create it and write the XML opening tag
      // if required. It definitely will exist if not appending.
      if LoggerFactory.fileDoesNotExist( currentLogFile ) {
         do {
            fileManager.createFile( atPath: currentLogFile, contents: Data(), attributes: [FileAttributeKey.posixPermissions: 0o666] )
            if layout.isXmlLayout() && firstWrite {
               try XmlLayout.openingTag.appendToFile( toFile: currentLogFile )
               firstWrite = false
            }
         } catch let error as NSError {
            throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile(name: currentLogFile, description: error.localizedDescription ) )
         }
      }
      
      // At this point there should be a log file set up to write to.
      // currentLogFile is always the file for writing to.
      // Now we have to check for the rolling files. Work out if the
      // file size is too big if we write to it.
      let attributes = try fileManager.attributesOfItem( atPath: currentLogFile )
      let fileSize = attributes[ FileAttributeKey.size ] as! Int
      let newFileSize = fileSize + formattedOutput.count
      
      // If the write does take it over the size limit then sort out backing up files.
      if newFileSize > maxFileSize  {
         
         // Start with the last one and work backwards losing the oldest (highest index).
         for idx in stride( from: maxBackupIndex, to: 0, by: -1 ) {
            let indexString = String( idx )
            
            // Set the destination file name either to xxx-n.ext or xxx.ext.n
            var destFileName = "\(currentLogFile).\(indexString)"
            if nameSuffix {
               destFileName = "\(!logFilePath.isEmpty ? "\(logFilePath)/" : "")\(logFileNameNoExtension)-\(indexString).\(logFileNameExtension)"
            }
            
            // Check whether it needs to be deleted if it exists
//            if LoggerFactory.fileExists( destFileName ){
//               do {
//                  try fileManager.removeItem( atPath: destFileName )
//               } catch let error as NSError {
//                  throw LoggerError.init( kind: ErrorKind.cannotDeleteLogFile(name: destFileName, description: error.localizedDescription ) )
//               }
//            }
            
            // idx counts down from the max backup index
            switch idx {
                  
               case 0 : ()
                  // Nothing needs to be done as this is the root log file.
                  
               case 1 :
                  // For example
                  //   currentLogFile = "hestia.xml"
                  //   destFileName = "hestia-1.xml"
                  //
                  // destFileName was deleted and currentLogFile is renamed to destFileName.
                  //
                  // Then a new currentLogFile is created.

                  let fileNameToMove = currentLogFile
                  
                  do {
                     if LoggerFactory.fileExists( fileNameToMove ) {
                        // Close down XML if required before moving it
                        if layout.isXmlLayout() {
                           try XmlLayout.closingTag.appendToFile( toFile: fileNameToMove )
                        }
                        try fileManager.moveItem( atPath: fileNameToMove, toPath: destFileName )
                        // print( "[\(idx)] fileNameToMove: \(fileNameToMove) -> destFileName: \(destFileName)" )
                     }
                  } catch let error as NSError {
                     throw LoggerError.init( kind: ErrorKind.cannotMoveLogFile(from: fileNameToMove, to: destFileName, description: error.localizedDescription) )
                  }
                  
                  fileManager.createFile( atPath: fileNameToMove, contents: Data(), attributes: [FileAttributeKey.posixPermissions: 0o666] )
                  if layout.isXmlLayout() {
                     try XmlLayout.openingTag.appendToFile( toFile: fileNameToMove )
                  }

               default :
                  // The top file needs to be deleted and the one underneath needs to be written to it.
                  //
                  // So for example
                  //   currentFileName = "hestia-1.xml"
                  //   destFileName = "hestia-2.xml"
                  //
                  // destFileName is deleted and currentFileNameis renamed to destFileName
                  //
                  // Then the currentFileName is created.

                  let prevIndexString = String( idx - 1 )
                  
                  // Set the current file name either to xxx-n.ext or xxx.ext.n
                  var fileNameToMove = "\(currentLogFile).\(prevIndexString)"
                  if nameSuffix {
                     fileNameToMove = "\(!logFilePath.isEmpty ? "\(logFilePath)/" : "")\(logFileNameNoExtension)-\(prevIndexString).\(logFileNameExtension)"
                  }
                  
                  do {
                     if LoggerFactory.fileExists( destFileName ) {
                        // print( "Removing: \(destFileName)" )
                        try fileManager.removeItem( atPath: destFileName )
                     }
                  } catch let error as NSError {
                     throw LoggerError.init( kind: ErrorKind.cannotDeleteLogFile(name: destFileName, description: error.localizedDescription ) )
                  }
                  do {
                     if LoggerFactory.fileExists( fileNameToMove ) {
                        try fileManager.moveItem( atPath: fileNameToMove, toPath: destFileName )
                        // print( "[\(idx)] fileNameToMove: \(fileNameToMove) -> destFileName: \(destFileName)" )
                     }
                  } catch let error as NSError {
                     throw LoggerError.init( kind: ErrorKind.cannotMoveLogFile(from: fileNameToMove, to: destFileName, description: error.localizedDescription) )
                  }
            } // hctiws
         } // rof
      } // fi
      
      // All the files have been moved around so we can safely write to the log file
      do {
         try formattedOutput.appendToFile( toFile: currentLogFile )
      } catch let error as NSError {
         throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile(name: currentLogFile, description: error.localizedDescription ) )
      }
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Close file assocated with appender.
   
   override func close() throws {
      var logFile = Appender.defaultLogFile
      logFile = try parameters.findParameter( Appender.fileAttrib ).value
      
      do {
         if layout.isXmlLayout() {
            try XmlLayout.closingTag.appendToFile( toFile: logFile )
         }
         
         let fileHandle: FileHandle? = FileHandle( forReadingAtPath: logFile )
         fileHandle?.closeFile()
      } catch let error as NSError {
         throw LoggerError.init( kind: ErrorKind.cannotWriteToLogFile(name: logFile, description: error.localizedDescription ) )
      }
   }
   

   
}


