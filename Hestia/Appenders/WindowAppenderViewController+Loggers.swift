//
//  WindowAppender+Loggers.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 04/06/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

extension WindowAppenderViewController {
   
   internal func loggerListContains( _ name: String ) -> Bool {
      for i in 0..<loggerMenuList.count {
         if name == loggerMenuList[i].name {
            return loggerMenuList[i].enabled
         }
      }
      return false
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

   internal func getLevelFor( _ name: String ) -> LoggerLevelEnum {
      for i in 0..<loggerMenuList.count {
         if name == loggerMenuList[i].name {
            return loggerMenuList[i].level
         }
      }
      return LoggerLevelEnum.off
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   internal func setLevelFor( _ name: String, to level: LoggerLevelEnum ) {
      print( "WindowAppenderViewController.setLevelFor( name: \(name), to: \(level.description)" )
      LoggerFactory.levelMessageData.name = name
      LoggerFactory.levelMessageData.level = level
      NotificationCenter.default.post( name: LoggerFactory.setLoggerLevelMessage, object: LoggerFactory.levelMessageData )

      // Search the list for update
      for i in 0..<loggerMenuList.count {
         if name == loggerMenuList[i].name {
            loggerMenuList[i].level = level
            return
         }
      }
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   internal func getColourFor( _ name: String ) -> NSColor {
      for i in 0..<loggerMenuList.count {
         if name == loggerMenuList[i].name {
            return loggerMenuList[i].colour
         }
      }
      return NSColor.white
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   internal func setDetailsForMenu( _ menu: Int, itemIndex: Int, title: String ) {
      let menuIndex = menu - 1
      if itemIndex == 0 {
         loggerMenuList[menuIndex].enabled = false
         loggerMenuList[menuIndex].name = ""
         levelMenuArray[menuIndex].isEnabled = false
      } else {
         loggerMenuList[menuIndex].enabled = true
         loggerMenuList[menuIndex].name = title
         levelMenuArray[menuIndex].isEnabled = true
      }
      // print( "\(menuIndex): \(loggerMenuList[menuIndex].name ): \(loggerMenuList[menuIndex].level.description)" )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // MARK: - Select Menu Action Methods
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_1(_ sender: Any) {
      let loggerName = selectMenu_1.title
      setDetailsForMenu( 1, itemIndex: selectMenu_1.indexOfSelectedItem, title: loggerName  )
      // print( "selectMenu_1.indexOfSelectedItem: \(selectMenu_1.indexOfSelectedItem)" )
      // print( "loggerName: '\(loggerName)'" )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      // print( "initialLevel: \(initialLevel), level title: \(level)" )
      levelMenu_1.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_2(_ sender: Any) {
      let loggerName = selectMenu_2.title
      setDetailsForMenu( 2, itemIndex: selectMenu_2.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_2.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_3(_ sender: Any) {
      let loggerName = selectMenu_3.title
      setDetailsForMenu( 3, itemIndex: selectMenu_3.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_3.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_4(_ sender: Any) {
      let loggerName = selectMenu_4.title
      setDetailsForMenu( 4, itemIndex: selectMenu_4.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_4.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_5(_ sender: Any) {
      let loggerName = selectMenu_5.title
      setDetailsForMenu( 5, itemIndex: selectMenu_5.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_5.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_6(_ sender: Any) {
      let loggerName = selectMenu_6.title
      setDetailsForMenu( 6, itemIndex: selectMenu_6.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_6.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_7(_ sender: Any) {
      let loggerName = selectMenu_7.title
      setDetailsForMenu( 7, itemIndex: selectMenu_7.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_7.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_8(_ sender: Any) {
      let loggerName = selectMenu_8.title
      setDetailsForMenu( 8, itemIndex: selectMenu_8.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_8.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_9(_ sender: Any) {
      let loggerName = selectMenu_9.title
      setDetailsForMenu( 9, itemIndex: selectMenu_9.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_9.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_10(_ sender: Any) {
      let loggerName = selectMenu_10.title
      setDetailsForMenu( 10, itemIndex: selectMenu_10.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_10.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_11(_ sender: Any) {
      let loggerName = selectMenu_11.title
      setDetailsForMenu( 11, itemIndex: selectMenu_11.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_11.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
   
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   
   @IBAction func selectMenuAction_12(_ sender: Any) {
      let loggerName = selectMenu_12.title
      setDetailsForMenu( 12, itemIndex: selectMenu_12.indexOfSelectedItem, title: loggerName  )
      let initialLevel = getInitialLogLevelFor( loggerName )
      let level = initialLevel.description
      levelMenu_12.selectItem( withTitle: level )
      setLevelFor( loggerName, to: initialLevel )
   }
}
