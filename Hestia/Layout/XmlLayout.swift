//
//  XmlLayout.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 14/10/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Handler for converting a layout pattern.
///
/// For example patterns such as
/// ````
/// <?xml version="1.0" encoding="UTF-8"?>
/// <logOutput xmlns="http://www.hsfr.org.uk/Schema/Hestia">
///    <log-event date="01/13/2018 05:31">
///       <data name="main.swift" line="24" function="setLoggingSystem()" level="Debug"/>
///       <message>Logger initialised</message>
///    </log-event>
///    <log-event date="01/13/2018 05:31">
///       <data name="main.swift" line="26" function="setLoggingSystem()" level="Debug"/>
///       <message>Version: Unkown version</message>
///    </log-event>
/// </logOutput>
/// ````
/// to a string suitable for output to the log.

class XmlLayout: Layout {

   let eventTag = "log-event"

   let dataTag = "data"
   let fileNameAttribute = "name"
   let lineAttribute = "line"
   let functionAttribute = "function"
   let levelAttribute = "level"

   let messageTag = "message"
   let dateAttribute = "date"

   let dateFormat = "MM/dd/yyyy hh:mm:ss"

   /// XML opening tag string
   static var openingTag: String {
      var header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      header += "<logOutput xmlns=\"http://www.hsfr.org.uk/Schema/Hestia\">\n"
      return header
   }

   /// XML closing tag string
   static var closingTag: String {
      let footer = "</logOutput>\n"
      return footer
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Convert log message to XML format.
   ///
   /// - Parameters:
   ///   - level: the logging level for this log message
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - message: the log message to output
   /// - Returns: the translated pattern

   override func formatLog( level: LoggerLevelEnum, fileName: String, line: Int, function: String, message: String ) -> String {
      var formattedString = ( parameters.list[ "ConversionPattern" ]?.value)!

      // Format the date
      let dateRegex = "(.*)(%d\\{(.+)\\})(.*)"
      let dateFormat = formattedString.replacingOccurrences( of: dateRegex, with: "$3", options: .regularExpression )
      let convertedDate = LoggerFactory.getCurrentDateString( format: dateFormat )
      formattedString = formattedString.replacingOccurrences( of: dateRegex, with: "$1\(convertedDate)$4", options: .regularExpression )

      let contents = LoggerFactory.outputTag( messageTag, contents:  message )
      let dataTag = LoggerFactory.outputTagWithAttributes( dataTag,
                                                           attrib: ( fileNameAttribute, fileName ),
                                                           ( lineAttribute, String( line ) ),
                                                           ( functionAttribute, function ),
                                                           ( levelAttribute, level.description.trimmingCharacters( in: .whitespaces ) ) )
      let eventTag = LoggerFactory.outputTagWithAttributes( eventTag, attrib: ( dateAttribute, convertedDate), contents: "\(dataTag)\(contents)")
      return eventTag
   }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Is this layout based on XML.
   ///
   /// - Returns: `false` unless overriden

   override func isXmlLayout() -> Bool {
      return true
   }

}

