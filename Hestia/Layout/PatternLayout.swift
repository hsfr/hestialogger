//
//  PatternLayout.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 27/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Handler for converting a layout pattern.
///
/// For example patterns such as
/// ````
///    %d{MM/dd/yyyy hh:mm}: %p %F [%L] %M: %m
/// ````
/// are converted to a string suitable for output to the log. Where
///
///    - `%d{}` formatted date
///    - `%F` file name containing the log statement
///    - `%M` function name where log statement occurs
///    - `%p` log level
///    - `%L` line number within file where log statement occurs
///    - `%m` log message

class PatternLayout: Layout {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Convert layout pattern to log message.
   ///
   /// - Parameters:
   ///   - level: the logging level for this log message
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - message: the log message to output
   /// - Returns: the translated pattern

   override func formatLog( level: LoggerLevelEnum, fileName: String, line: Int, function: String, message: String ) -> String {
      var formattedString = ( parameters.list[ "ConversionPattern" ]?.value)!

      // Format the date
      let dateRegex = "(.*)(%d\\{(.+)\\})(.*)"
      let dateFormat = formattedString.replacingOccurrences( of: dateRegex, with: "$3", options: .regularExpression )
      let convertedDate = LoggerFactory.getCurrentDateString( format: dateFormat )
      formattedString = formattedString.replacingOccurrences( of: dateRegex, with: "$1\(convertedDate)$4", options: .regularExpression )

      // Format the file name
      let fileRegex = "(.*)%F(.*)"
      formattedString = formattedString.replacingOccurrences( of: fileRegex, with: "$1\(fileName)$2", options: .regularExpression )

      // Format the function name
      let functionRegex = "(.*)%M(.*)"
      formattedString = formattedString.replacingOccurrences( of: functionRegex, with: "$1\(function)$2", options: .regularExpression )

      // Format the line number
      let lineRegex = "(.*)%L(.*)"
      formattedString = formattedString.replacingOccurrences( of: lineRegex, with: "$1\(line)$2", options: .regularExpression )

      // Format the level
      let levelRegex = "(.*)%p(.*)"
      formattedString = formattedString.replacingOccurrences( of: levelRegex, with: "$1\(level.description)$2", options: .regularExpression )

      // Format the message
      let messageRegex = "(.*)%m(.*)"
      formattedString = formattedString.replacingOccurrences( of: messageRegex, with: "$1\(message)$2", options: .regularExpression )

      return formattedString
   }
}
