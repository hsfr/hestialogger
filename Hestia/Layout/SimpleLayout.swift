//
//  SimpleLayout.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 14/10/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Handler for converting a log message to a simple format.
///
/// For example
/// ```
///    Info - Logger initialised
/// ```

class SimpleLayout: Layout {

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Convert layout pattern to sinoke log message.
   ///
   /// - Parameters:
   ///   - level: the logging level for this log message
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - message: the log message to output
   /// - Returns: the translated pattern

   override func formatLog( level: LoggerLevelEnum, fileName: String, line: Int, function: String, message: String ) -> String {
      return "\(level.description) - \(message)"
   }
}

