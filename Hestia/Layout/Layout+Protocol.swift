//
//  Layout+Protocol.swift
//  HestiaLogger
//
//  Created by Hugh Field-Richards on 01/06/2020.
//  Copyright © 2020 Hugh Field-Richards. All rights reserved.
//

import Foundation

typealias LayoutListType = [ String: LayoutProtocol ]

protocol LayoutsProtocol {

   /// A list of layouts indexed by class name
   var layoutList: LayoutListType { get set }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   /// Find the layout in the list. If it is
   /// not in the list it throws an error.
   ///
   /// - Parameters:
   ///    - name: name of layout to fetch
   /// - Returns: Found layout
   /// - Throws: `LoggerError` if layout is not found

   func findLayout( _ name: String ) throws -> LayoutProtocol

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// The protocol for the layouts.

protocol LayoutProtocol {

   /// The name of the layout class derived from the XML, for example
   /// ````
   ///     <layout class="XmlLayout"/>
   /// ````
   var layoutClassName: String { get set }

   /// A list of parameters for this layout
   var parameters: ParametersProtocol { get set }

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Format layout pattern to log message.
   ///
   /// - Parameters:
   ///   - level: the logging level for this log message
   ///   - fileName: the file name where the log statement is
   ///   - line: the line number of the log statement in the file
   ///   - function: the function where the log statement resides
   ///   - message: the log message to output
   /// - Returns: the translated pattern

   func formatLog( level: LoggerLevelEnum, fileName: String, line: Int, function: String, message: String ) -> String

   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   // -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
   //
   /// Is this layout based on XML.
   ///
   /// - Returns: `false` unless overriden

   func isXmlLayout() -> Bool

}


