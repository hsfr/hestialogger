//
//  LoggerLevel.swift
//  Hestia
//
//  Created by Hugh Field-Richards on 25/09/2017.
//  Copyright © 2019 Hugh Field-Richards. All rights reserved.
//

import Foundation

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Enumeration Types
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// LoggerLevel defines the precedence of the log request.
///
/// The order of the levels (similar to log4j) is:
///
/// `trace < debug < info < warn < error < fatal < off`

public enum LoggerLevelEnum: Int {

   case trace = 0, debug, info, warn, error, fatal, off

   static let count = 7
   
   // Description string of level. Note that the string length is
   // constant so that padding is used for "prettier" output.
   var description : String {
      switch( self ) {
         case .trace:
            return "Trace"
         case .debug:
            return "Debug"
         case .info:
            return " Info"
         case .warn:
            return " Warn"
         case .error:
            return "Error"
         case .fatal:
            return "Fatal"
         case .off:
            return "  Off"
      }
   }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// MARK: - Protocol Definitions
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*

protocol LevelProtocol {

   /// Defined in the configuration file as <level value="...">
   var value: String { get set }

   /// The raw value of the level.
   var rawValue: Int { get }

   /// The raw value of the level.
   var enumValue: LoggerLevelEnum { get }

}

// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
// -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
//
/// Levels determine whether a particular logging statement is successful.
///
/// There are 7 levels (identical to *log4j*):
///
///  - trace = 0
///  - debug = 1
///  - info = 2
///  - warn = 3
///  - error = 4
///  - fatal = 5
///  - off = 6
///
///  The logger level determines that all logging statements which have an
///  associated value less than the level will not be output. For example
///  if a logger's level is defined as `warn` then a `logger.info()`
///  statement will be ignored. The `off` level is a convenience to
///  inhibit a logger either from the configuration
///  file or set in the code.

public struct Level: LevelProtocol {

   static public let trace = LoggerLevelEnum.trace
   static public let debug = LoggerLevelEnum.debug
   static public let info = LoggerLevelEnum.info
   static public let warn = LoggerLevelEnum.warn
   static public let error = LoggerLevelEnum.error
   static public let fatal = LoggerLevelEnum.fatal
   static public let off = LoggerLevelEnum.off

   public var value: String = ""

   public var rawValue: Int {
      return enumValue.rawValue
   }

   public var enumValue: LoggerLevelEnum {
      switch value {
         case "trace":
            return LoggerLevelEnum.trace
         case "debug":
            return LoggerLevelEnum.debug
         case "info":
            return LoggerLevelEnum.info
         case "warn":
            return LoggerLevelEnum.warn
         case "error":
            return LoggerLevelEnum.error
         case "fatal":
            return LoggerLevelEnum.fatal
         default:
            return LoggerLevelEnum.off
      }
   }
}

